// ────────────────────────────────────────────
// file:        task.cpp
// project:     code
// author:      Timur Barbashov            𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
// e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
// September 25, 2020
// ────────────────────────────────────────────
#include <chrono>
#include <iostream>
#include <math.h>
#include <mpi.h>
#include <random>
#include <stdio.h>
using namespace std;

/**
 * @brief Get the world data
 * Для малюсенького упрощения жизни и сокращения кода
 *
 * @param world_rank
 * @param world_size
 **/
void
get_world_data(int& world_rank, int& world_size) {
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
}

void
task_first() {
  int world_size, world_rank, processor_name_len;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  // procs count | threads count
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // procs rank | thread number
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // processor name
  MPI_Get_processor_name(processor_name, &processor_name_len);
  std::cout << "[" << processor_name << "]: Have a good day, Dinara Halilovna!"
            << "  from  [" << world_rank << "]  of  [" << world_size << "]" << endl;
}

void
task_second() {
  // Не обращайте внимания на генерацию рандомных чисел,
  //    это сделано для более компактного вывода
  //    без многозначных чисел
  const int ARRAY_SIZE = 10;
  int a[ARRAY_SIZE];
  int world_size, world_rank;
  get_world_data(world_rank, world_size);

  if (world_rank == 0) {
    for (auto i = 0; i < ARRAY_SIZE; ++i)
      a[i] = rand() / (RAND_MAX / 5);
    MPI_Send(&a, ARRAY_SIZE, MPI_INT, 1, 0, MPI_COMM_WORLD);
  }
  else if (world_rank == 1) {
    MPI_Recv(&a, ARRAY_SIZE, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    std::cout << "[" << world_rank << "]"
              << "  received array:" << std::endl
              << "    ";
    for (auto i = 0; i < ARRAY_SIZE; ++i)
      std::cout << a[i] << " ";
    std::cout << endl << "from  [0]";
  }
}

/**
 * @brief Поэлементные сумма и произведение векторов
 *
 **/
void
task_fourth() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);

  int chunk_size = 5;
  int ARRAY_SIZE = chunk_size * (world_size - 1);
  int x[ARRAY_SIZE], y[ARRAY_SIZE], z_sum[ARRAY_SIZE], z_mult[ARRAY_SIZE];
  int x_local[chunk_size], y_local[chunk_size], z_local_sum[chunk_size], z_local_mult[chunk_size];

  if (world_rank == 0) {
    // Инициализация
    for (auto i = 0; i < ARRAY_SIZE; ++i) {
      x[i] = rand() / (RAND_MAX / chunk_size);
      y[i] = rand() / (RAND_MAX / chunk_size);
    }

    std::cout << "[x]:       ";
    for (auto& x_i : x)
      std::cout << x_i << " ";
    std::cout << std::endl << "[y]:       ";
    for (auto& y_i : y)
      std::cout << y_i << " ";
    std::cout << std::endl << "--------" << std::endl;

    // Передача, по сути ручная реализация Stride
    for (auto i = 1; i < world_size; ++i) {
      const int idx = (i - 1) * chunk_size;
      MPI_Send(&x[idx], chunk_size, MPI_INT, i, 0, MPI_COMM_WORLD);
      MPI_Send(&y[idx], chunk_size, MPI_INT, i, 1, MPI_COMM_WORLD);
    }

    // Приём, тоже по сути ручной Stride
    for (auto i = 1; i < world_size; ++i) {
      const int idx = (i - 1) * chunk_size;
      MPI_Recv(&z_sum[idx], chunk_size, MPI_INT, i, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(&z_mult[idx], chunk_size, MPI_INT, i, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    std::cout << "[z_sum]:   ";
    for (const int& zi : z_sum)
      std::cout << zi << " ";
    std::cout << std::endl;
    std::cout << "[z_mult]:  ";
    for (const int& zi : z_mult)
      std::cout << zi << " ";
    std::cout << std::endl;
  }
  else {
    MPI_Recv(&x_local, chunk_size, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&y_local, chunk_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    for (auto i = 0; i < chunk_size; ++i) {
      z_local_sum[i] = x_local[i] + y_local[i];
      z_local_mult[i] = x_local[i] * y_local[i];
    }
    MPI_Send(&z_local_sum, chunk_size, MPI_INT, 0, 2, MPI_COMM_WORLD);
    MPI_Send(&z_local_mult, chunk_size, MPI_INT, 0, 3, MPI_COMM_WORLD);
  }
}

/**
 * @brief Умножение матрицы (n×n) на вектор (n) и выделение диагонали матрицы
 * Матрица хранится не наивным способом (массив указателей),
 *    а в развёрнутом - массив размера n², типа Фортран-стайл
 * По идее, так должно быть сильно быстрее в смысле затрат времени на пересылку
 * Вся работа так же через Stride
 * По части индексации, чувствую, можно было сделать всё гоаздо красивее,
 *    но переделывать было больно
 **/
void
task_fifth() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);
  const int SIZE = 6;
  const int chunk_size = SIZE / (world_size - 1);
  int matrix[SIZE * SIZE], matrix_local[SIZE * chunk_size];
  int vec[SIZE], vec_local[SIZE], result[SIZE], result_local[SIZE];
  int diag[SIZE], diag_local[SIZE];

  if (world_rank == 0) {
    for (auto i = 0; i < SIZE * SIZE; ++i)
      matrix[i] = rand() / (RAND_MAX / 10);
    for (auto i = 0; i < SIZE; ++i)
      vec[i] = rand() / (RAND_MAX / 10);

    // Вывод инциализированных вектора и матрицы
    printf("[vector]:\n    (");
    for (auto& vec_i : vec)
      printf("%d ", vec_i);
    printf(")\n");
    printf("[matrix]:\n");
    for (auto i = 0; i < SIZE; ++i) {
      printf("     ");
      for (auto j = 0; j < SIZE; ++j)
        printf("%d  ", matrix[i * SIZE + j]);
      printf("\n");
    }
    printf("\n");

    // Рассылка данных
    // Для умножения каждой строки нужен целый вектор,
    //    поэтому порционно передаём только матрицу
    for (auto i = 1; i < world_size; ++i) {
      const int idx = (i - 1) * SIZE * chunk_size;
      MPI_Send(&vec, SIZE, MPI_INT, i, 0, MPI_COMM_WORLD);
      MPI_Send(&matrix[idx], chunk_size * SIZE, MPI_INT, i, 1, MPI_COMM_WORLD);
      MPI_Send(&result, SIZE, MPI_INT, i, 2, MPI_COMM_WORLD);
      MPI_Send(&diag, SIZE, MPI_INT, i, 4, MPI_COMM_WORLD);
    }

    // Приём результата
    for (auto i = 1; i < world_size; ++i) {
      const int idx = (i - 1) * chunk_size;
      MPI_Recv(&result[idx], chunk_size, MPI_INT, i, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(&diag[idx], chunk_size, MPI_INT, i, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // Вывод результирующего вектора и диагонали
    printf("[Ax]:\n    (");
    for (auto& result_i : result)
      printf("%d ", result_i);
    printf(")\n\n");
    printf("[diag(A)]:\n    (");
    for (auto& diag_i : diag)
      printf("%d ", diag_i);
    printf(")");
  }
  else {
    // Приём данных. Комментарий без комментариев
    MPI_Recv(&vec_local, SIZE, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&matrix_local, SIZE * chunk_size, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&result_local, SIZE, MPI_INT, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&diag_local, SIZE, MPI_INT, 0, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    // Умножение части матрицы на вектор
    // Каждый процесс вычисляет свою часть (позиционно) результирующего вектора
    const int idx = (world_rank - 1) * chunk_size;
    for (auto i = 0; i < chunk_size; ++i) {
      int sum = 0;
      for (auto j = 0; j < SIZE; ++j) {
        sum += matrix_local[i * SIZE + j] * vec_local[j];
      }
      result_local[idx + i] = sum;
    }

    // Выделение главной диагонали в плоской матрице
    // Индексацию придумывал честно сам. Жесть
    for (auto i = 0; i < chunk_size; ++i) {
      // for (auto j = 0; j < chunk_size; ++j)
      diag_local[idx + i] = matrix_local[i * SIZE + i + (world_rank - 1) * chunk_size];
    }

    // Пересылка данных
    MPI_Send(&result_local[idx], chunk_size, MPI_INT, 0, 3, MPI_COMM_WORLD);
    MPI_Send(&diag_local[idx], chunk_size, MPI_INT, 0, 4, MPI_COMM_WORLD);
  }
}

/**
 * @brief Ping-Pong
 * Запускать с -np 2
 * Для каждого объёма данных (n) проводится 10 тестов
 *    и высчитывается среднее время выполнения
 * В выводе среднего времени указано число байт в сообщении
 * @param sum
 **/
void
task_sixth_pingpong(long& sum) {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);

  for (auto n = 1; n < 9; ++n) {
    int data[n];

    for (auto i = 0; i < 10; ++i) {
      if (world_rank == 0) {
        for (auto& i : data)
          i = rand();
        MPI_Send(&data, n, MPI_INT, 1, 0, MPI_COMM_WORLD);

        auto start = std::chrono::high_resolution_clock::now();
        MPI_Recv(&data, n, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        printf("%ld ", duration.count());
        sum += duration.count();
      }
      else {
        MPI_Recv(&data, n, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Send(&data, n, MPI_INT, 0, 0, MPI_COMM_WORLD);
      }
    }
    if (world_rank == 0) {
      printf("[%lu][Average]:  %ld μs\n", sizeof(data), sum / 10);
      sum = 0;
    }
  }
}

/**
 * @brief Ping-Ping
 * Запускать с -np 2
 * Для каждого объёма данных (n) проводится 10 тестов
 *    и высчитывается среднее время выполнения
 * В выводе среднего времени указано число байт в сообщении
 * @param sum
 **/
void
task_sixth_pingping(long& sum) {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);

  for (auto n = 1; n < 9; ++n) {
    int data[n];

    for (auto i = 0; i < 10; ++i) {
      if (world_rank == 0) {
        for (auto& i : data)
          i = rand();
        MPI_Send(&data, n, MPI_INT, 1, 0, MPI_COMM_WORLD);

        auto start = std::chrono::high_resolution_clock::now();
        MPI_Recv(&data, n, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        printf("[%d] received in %ld μs\n", world_rank, duration.count());
        sum += duration.count();
      }
      else {
        auto start = std::chrono::high_resolution_clock::now();
        MPI_Recv(&data, n, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        auto end = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        printf("[%d] received in %ld μs\n", world_rank, duration.count());
        sum += duration.count();
        MPI_Send(&data, n, MPI_INT, 0, 0, MPI_COMM_WORLD);
      }
    }
    if (world_rank == 0) {
      printf("[%lu][Total average]:  %ld μs\n\n", sizeof(data), sum / 10);
      sum = 0;
    }
  }
}

/**
 * @brief Норма вектора со Scatter и Reduce
 * ||x||₁ = ∑|xᵢ|
 **/
void
task_seventh() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);
  const int SIZE = 8;
  const int chunk_size = SIZE / world_size;
  double x[SIZE], x_local[SIZE];
  double norm = 0.0, norm_local = 0.0;

  std::cout.precision(3);
  if (world_rank == 0) {
    for (auto i = 0; i < SIZE; ++i)
      x[i] = abs((double)rand() / (RAND_MAX / 10));

    std::cout << "[x]:" << std::endl << "    ( ";
    for (auto& x_i : x)
      std::cout << x_i << " ";
    std::cout << ")" << std::endl;
  }

  MPI_Scatter(&x, chunk_size, MPI_DOUBLE, &x_local, chunk_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (auto i = 0; i < chunk_size; ++i)
    norm_local += x_local[i];
  std::cout << "[" << world_rank << "] local norm:  " << norm_local << std::endl;

  MPI_Reduce(&norm_local, &norm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if (world_rank == 0) {
    std::cout << "||x||₁:  " << norm;
  }
}

/**
 * @brief Скалярное произведение векторов через Scatter & Reduce
 *
 **/
void
task_eighth() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);
  const int SIZE = 8;
  const int chunk_size = SIZE / world_size;
  double x[SIZE], y[SIZE], x_local[SIZE], y_local[SIZE];
  double dot = 0.0, dot_local = 0.0;
  std::cout.precision(3);

  if (world_rank == 0) {
    std::cout << "[x]:\n    ( ";
    for (auto& x_i : x) {
      x_i = (double)rand() / (RAND_MAX / 10);
      std::cout << x_i << " ";
    }
    std::cout << ")\n[y]:\n    (";
    for (auto& y_i : y) {
      y_i = (double)rand() / (RAND_MAX / 10);
      std::cout << y_i << " ";
    }
    std::cout << " )\n";
  }

  MPI_Scatter(&x, chunk_size, MPI_DOUBLE, &x_local, chunk_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatter(&y, chunk_size, MPI_DOUBLE, &y_local, chunk_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (auto i = 0; i < chunk_size; ++i)
    dot_local += x_local[i] * y_local[i];

  MPI_Reduce(&dot_local, &dot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (world_rank == 0)
    std::cout << "[x⋅y]:  " << dot;
}

/**
 * @brief Поиск минимума и его индекса через MPI_MINLOC
 *
 */
void
task_ninth() {
  int world_rank, world_size, min_rank, min_index;
  get_world_data(world_rank, world_size);
  const int SIZE = 12;
  double min_value;
  double array[SIZE];
  std::cout.precision(2);

  // Структура для пары значение-индекс, требуемая для работы MINLOC/MAXLOC
  struct {
    double value;
    int index;
  } input, output;

  for (auto& a_i : array)
    a_i = (double)rand() / (RAND_MAX / 10);

  // Локальный поиск минимума на каждом процессе
  input.value = array[0];
  input.index = 0;
  for (auto i = 1; i < SIZE; ++i) {
    if (input.value > array[i]) {
      input.value = array[i];
      input.index = i;
    }
  }

  MPI_Reduce(&input, &output, 1, MPI_DOUBLE_INT, MPI_MINLOC, 0, MPI_COMM_WORLD);
  if (world_rank == 0) {
    min_value = output.value;
    min_rank = output.index / SIZE;
    min_index = output.index % SIZE;
    for (auto& a_i : array)
      std::cout << a_i << "  ";
    std::cout << endl;
    std::cout << "Min value:  " << min_value << std::endl << "Min value index:  " << min_index;
  }
}

/**
 * @brief Умножение матрицы на вектор
 *    с использованием Scatter, Gather и Bcast
 * Часть кода взята из задания 5, в котором по сути была ручная реализация данной задачи
 *
 */
void
task_tenth() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);
  const int SIZE = 6;
  const int chunk_size = SIZE / (world_size);

  int vec[SIZE], vec_result[SIZE], vec_result_local[chunk_size];
  int matrix[SIZE * SIZE], matrix_local[SIZE * SIZE];

  if (world_rank == 0) {
    for (auto& matrix_i : matrix)
      matrix_i = rand() / (RAND_MAX / 10);
    for (auto& vec_i : vec)
      vec_i = rand() / (RAND_MAX / 10);

    // Вывод инициализированных вектора и матрицы
    std::cout << "[x]:\n    ( ";
    for (auto vec_i : vec)
      std::cout << vec_i << " ";
    std::cout << ")" << std::endl;
    std::cout << "[A]:" << std::endl;
    for (auto i = 0; i < SIZE; ++i) {
      std::cout << "    ";
      for (auto j = 0; j < SIZE; ++j)
        std::cout << matrix[i * SIZE + j] << "  ";
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
  for (auto& vec_result_local_i : vec_result_local)
    vec_result_local_i = 0;

  // Рассылка данных через Scatter
  MPI_Scatter(&matrix, SIZE * chunk_size, MPI_INT, &matrix_local, SIZE * chunk_size, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&vec, SIZE, MPI_INT, 0, MPI_COMM_WORLD);

  for (auto i = 0; i < chunk_size; ++i)
    for (auto j = 0; j < SIZE; ++j)
      vec_result_local[i] += matrix_local[i * SIZE + j] * vec[j];

  MPI_Gather(&vec_result_local, chunk_size, MPI_INT, &vec_result, chunk_size, MPI_INT, 0, MPI_COMM_WORLD);

  if (world_rank == 0) {
    std::cout << "[A⋅x]:" << std::endl << "    ( ";
    for (auto vec_result_i : vec_result)
      std::cout << vec_result_i << " ";
    std::cout << ")";
  }
}

/**
 * @brief Рассылка чётных-нечётных строк матрицы
 *
 */
void
task_eleventh() {
  int world_rank, world_size;
  get_world_data(world_rank, world_size);
  const int SIZE = 8;
  const int chunk_size = SIZE / (world_size);
  int a[SIZE][SIZE];
  MPI_Datatype matrix_column_type;

  // Про перечисление подсмотрел в каком-то крутом разборе MPI
  enum roles { SENDER, RECEIVER };
  switch (world_rank) {
    case SENDER: {
      // Инициализация и вывод исходной матрицы
      std::cout << "[A]:" << std::endl;
      for (auto& a_i : a) {
        std::cout << "    ";
        for (auto& a_i_j : a_i) {
          a_i_j = rand() / (RAND_MAX / 10);
          std::cout << a_i_j << "  ";
        }
        std::cout << std::endl;
      }

      MPI_Datatype matrix_column_type;
      MPI_Type_vector(SIZE / 2, SIZE, SIZE * 2, MPI_INT, &matrix_column_type);
      MPI_Type_commit(&matrix_column_type);
      MPI_Send(&a[0][0], 1, matrix_column_type, RECEIVER, 0, MPI_COMM_WORLD);
      break;
    }
    case RECEIVER: {
      int b[SIZE / 2][SIZE], c[SIZE / 2][SIZE];
      MPI_Recv(&b[0][0], 1, MPI_INT, SENDER, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      std::cout << "[B]:" << std::endl;
      for (auto& b_i : b) {
        std::cout << "    ";
        for (auto& b_i_j : b_i)
          std::cout << b_i_j << "  ";
        std::cout << std::endl;
        break;
      }
    }
  }
}
int
main(int argc, char** argv) {
  // srand() здесь для задания случайного seed при рандоме
  srand(time(nullptr));

  // MPI init
  MPI_Init(&argc, &argv);

  // Первый блок заданий, сдан очно
  // task_first();
  // task_second();
  // task_third();

  // Второй блок заданий
  //    Насчёт корректного измерения времени на Windows
  //        гарантий, к сожалению, давать не могу,
  //        так как таймеры в ней работаю иначе
  // Теперь рабочее, смотрите на здоровье
  // task_fourth();
  // task_fifth();
  long sum = 0;
  // task_sixth_pingpong(sum);
  // task_sixth_pingping(sum);

  // Третий блок заданий. 7, 8 сданы раньше
  // task_seventh();
  // task_eighth();

  // task_ninth();
  // task_tenth();

  // Недоделано
  // task_eleventh();

  // flush resources
  MPI_Finalize();
  return 0;
}
