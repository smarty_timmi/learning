use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Угадай число");
    let number = rand::thread_rng().gen_range(1..=20);
    loop {
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("Ну не тооо");
        let guess: u32 = match guess.trim().parse() {
            Ok(number) => number,
            Err(_) => continue,
        };
        match guess.cmp(&number) {
            Ordering::Less => println!("Недобор"),
            Ordering::Greater => println!("Перебор!"),
            Ordering::Equal => {
                println!("Выиграл!");
                break;
            }
        };
    }
    // println!("Загаданное число: {}", number);
}
