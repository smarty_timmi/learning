use std::io;

fn fibo(n: i32) -> i32 {
    if n >= 3 {
        let mut fibo_prev: i32 = 1;
        let mut fibo_current: i32 = 1;
        let mut result: i32 = 0;
        for _i in 3..=n {
            result = fibo_current + fibo_prev;
            fibo_prev = fibo_current;
            fibo_current = result;
        }
        return result;
    } else {
        return 1;
    }
}

fn main() {
    println!("Fibonacci numbers to: ");
    let mut n = String::new();
    io::stdin().read_line(&mut n).expect("Must be an integer");
    let n: i32 = n.trim().parse().expect("Must be an Integer");
    let number: i32 = fibo(n);
    println!("Result: {}", number);
}
