# ────────────────────────────────────────────
# file:        spells.py
# project:     pydnd
# Author:      Timur Barbashov        𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# April 17th 2021
# ────────────────────────────────────────────
from typing import Tuple, NoReturn


class WizardSpells:
    spells_capacity_table: Tuple = (
        (2, 0, 0, 0, 0, 0, 0, 0, 0),
        (3, 1, 0, 0, 0, 0, 0, 0, 0),
        (4, 2, 1, 0, 0, 0, 0, 0, 0),
        (4, 3, 1, 1, 0, 0, 0, 0, 0),
        (4, 3, 2, 1, 1, 0, 0, 0, 0),
        (4, 3, 3, 1, 1, 1, 0, 0, 0),
        (5, 3, 3, 2, 1, 1, 1, 0, 0),
        (5, 4, 3, 2, 1, 1, 1, 1, 0),
        (5, 4, 3, 3, 1, 1, 1, 1, 1),
        (5, 4, 3, 3, 2, 1, 1, 1, 1),
        (5, 4, 3, 3, 2, 1, 1, 1, 1),
        (5, 5, 3, 3, 2, 1, 1, 1, 1),
        (5, 5, 4, 3, 2, 1, 1, 1, 1),
        (5, 5, 4, 4, 2, 1, 1, 1, 1),
        (5, 5, 4, 4, 3, 1, 1, 1, 1),
        (6, 5, 4, 4, 3, 2, 1, 1, 1),
        (6, 5, 5, 4, 3, 2, 1, 1, 1),
        (6, 5, 5, 4, 3, 2, 1, 1, 1),
        (6, 5, 5, 4, 3, 2, 2, 1, 1),
        (6, 6, 5, 4, 3, 2, 2, 1, 1),
    )

    def spells_capacity(self, level: int):
        return self.spells_capacity_table[level - 1]


class Spells:
    def secret_restoration(self) -> NoReturn:
        pass

    def weaving(self) -> NoReturn:
        pass

    def magical_tradition(self) -> NoReturn:
        pass

    def mages_knowledge(self) -> NoReturn:
        pass