# ────────────────────────────────────────────
# file:        races.py
# project:     pydnd
# Author:      Timur Barbashov        𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# April 17th 2021
# ────────────────────────────────────────────
from spells import Spells
from dataclasses import dataclass, field


@dataclass()
class Race:

    inspiraton: int = field(default=0, init=False)
    strength: int = field(default=0, init=False)
    dexterity: int = field(default=0, init=False)
    constitution: int = field(default=0, init=False)
    intelligence: int = field(default=0, init=False)
    wisdom: int = field(default=0, init=False)
    charisma: int = field(default=0, init=False)
    perception: int = field(default=0, init=False)
    speed: int = field(default=30, init=False)

    def __post_init__(self):
        self.__slots__ = [
            "inspiraton",
            "strength",
            "dexterity",
            "constitution",
            "intelligence",
            "wisdom",
            "charisma",
            "perception",
            "speed",
            "asi",
            "__weakref__",
        ]


@dataclass()
class Dragonborn(Race):
    strength: int = 2
    charisma: int = 1


@dataclass()
class Dwarf(Race):
    constitution: int = 2
    speed: int = 25


@dataclass()
class Elf(Race):
    dexterity: int = 2


@dataclass()
class Gnome(Race):
    speed: int = 25


# TODO не забыть при создании требовать поля, к которым добавлять asi
@dataclass()
class HalfElf(Race):
    charisma: int = 2
    asi: int = 2


@dataclass()
class Halfling(Race):
    speed: int = 25


@dataclass()
class HalfOrc:
    strength: int = 2
    constitution: int = 1


@dataclass()
class Human(Race):
    inspiraton: int = 1
    strength: int = 1
    dexterity: int = 1
    constitution: int = 1
    intelligence: int = 1
    wisdom: int = 1
    charisma: int = 1
    perception: int = 1


@dataclass()
class Tiefling:
    charisma: int = 2
    intelligence: int = 1
