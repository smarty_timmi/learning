# ────────────────────────────────────────────
# file:        pydnd.py
# project:     Projects
# Author:      Timur Barbashov        𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# April 3rd 2021
# ────────────────────────────────────────────
# TODO 1 абстрактный класс Персонаж, от которого наследуются Классы персонажей
# TODO 1 абстрактный класс Враг, от которого наследуются все враги
# TODO 1 класс для гейммастера
# TODO 1 характеристики, дающие бонусы к перкам, переписать на DataClass`ах
# TODO 2 этапы прохождения в виде наборов комнат/врагов/ситуаций
# TODO 3 GUI
# TODO 3 "таймеры" на временные эффекты в бою и т.д.
# TODO 3 Варианты для рас
# TODO 4 база данных игры с логами событий
# TODO 4 виртуальное поле с координатами и размером шага, проверками расстояния для передвижения
# TODO 5 скриптование пользовательских функций/классов
# TODO 7 базовые экземпляры "Персонажей-примеров"
#! Переписать перки/расы/спеллы(?) на датаклассы
#! МетаКласс для создания Character, в new можно передать список атрибутов

from abc import ABC, ABCMeta
from dataclasses import dataclass, field, asdict
from typing import Dict, List, NoReturn, Optional, Union, Type
from races import Race, Human
import spells
from dices import *
from pprint import pp, pprint

# TODO класс PerksManager и SpellsManager для обновления значений и сокращения кода (по сути внутри вызывает .asdict для обеих экземпляров)


# Perks[PerksEngine] обёртка для датакласса, в целом только для удобства раздачи параметров полям (через словари)
@dataclass
class Perks:
    @dataclass
    class PerksEngine:
        inspiraton: int = field(default=0)
        strength: int = field(default=0)
        dexterity: int = field(default=0)
        constitution: int = field(default=0)
        intelligence: int = field(default=0)
        wisdom: int = field(default=0)
        charisma: int = field(default=0)
        perception: int = field(default=0)

        def __post_init__(self):
            self.__slots__ = [
                "inspiraton",
                "strength",
                "dexterity",
                "constitution",
                "intelligence",
                "wisdom",
                "charisma",
                "perception",
            ]

    def update(self, other: Human):
        # pprint(f"{self.__slots__=}")
        # pprint(f"{other.__slots__=}")
        pass


h = Human()
p = Perks(1, 2, 3, 4, 5, 6, 7, 8)
pprint(asdict(p))
pprint(asdict(h))

# * Для каждого стандартного класса персонажа список "целевых атрибутов" -- основные характеристики для класса персонажа, от которых скейлятся умения
# TODO __init__(char_class)
# TODO метод переопределения "целевых атрибутов"
class Character:
    """
    Character base class

    Instance parameters
    ----------
    name : str
        Имя
    race : str
        Раса
    char_class : str
        Класс
    gender : str
        Пол
    level : int
        Уровень
    personality : str

    history : str
        История
    alignment : str
    hp_limit : int
        Максимальные хиты
    hp : int
        Текущие хиты
    hp_temp : int
        Временные хиты
    exp : int
        Опыт

    Perks
    ----------
    inspiration : int
        Вдохновение
    strength : int
        Сила
    dexterity : int
        Ловкость
    constitution : int
        Телосложение
    intelligence : int
        Интеллект
    wisdom : int
        Мудрость
    charisma : int
        Харизма
    perception : int
        Проницательность
    speed : int
        Скорость
    armor_class : int
        Класс брони

    Temporary
    ----------
    languages : List[str]
        Языки
    death_saves : List[str]
    initiative : int
        Инициатива

    Coins
    ----------
    cp : int
    sp : int
    ep : int
    gp : int
    pp : int

    Spells
    ----------
    spells : Dict
        Заклинания (включая заговоры)
    spells_capacity : List[int]
    spellc_class : str
    spellc_ability : str
    spell_attack_bonus : int
    spell_save_dc : str

    Items
    ----------
    items : Dict
    """

    personality: str
    history: str
    alignment: str
    hp_limit: int
    hp: int
    hp_temp: int
    # ---- perks
    perks: Dict[str, int] = {
        "inspiraton": 0,
        "strength": 0,
        "dexterity": 0,
        "constitution": 0,
        "intelligence": 0,
        "wisdom": 0,
        "charisma": 0,
        "perception": 0,
    }
    # inspiration: int
    # strength: int
    # dexterity: int
    # constitution: int
    # intelligence: int
    # wisdom: int
    # charisma: int
    # perception: int
    # ---- perks
    languages: List[str]
    death_saves: List[int]
    armor_class: int = 0
    initiative: int = 0
    speed: int
    cp: int = 0
    sp: int = 0
    ep: int = 0
    gp: int = 0
    pp: int = 0

    # TODO переписать на датаклассе/классе Spell с доп. информацией
    spells: Dict
    spells_capacity: List[int]
    spellc_class: str
    spellc_ability: str
    spell_attack_bonus: int
    spell_save_dc: str

    # TODO переписать на датаклассе/классе Items с доп. информацией
    items: Dict

    def __init__(
        self,
        name: str,
        race: Type[Race],
        gender: str,
        level: int,
        char_class: str,
        exp: Optional[int] = None,
    ):
        try:
            self.race: Type[Race] = race
        except:
            raise ValueError(race)
        if level != 0:
            self.level = level
        else:
            raise ValueError(level)
        self.char_class = char_class
        self.name: str = name
        self.gender: str = gender
        self.exp: int = exp if exp is not None else 0


class Barbarian:
    pass


class Bard:
    pass


class Cleric:
    pass


class Druid:
    pass


class Fighter:
    pass


class Monk:
    pass


class Paladin:
    pass


class Ranger:
    pass


class Rogue:
    pass


class Sorcerer:
    pass


class Warlock:
    pass


class Wizard(Character):
    def __init__(self, name: str, race: Type[Race], gender: str, level: int):
        super().__init__(name, race, gender, level, "wizard")
        self.level = level
        self.spells_capacity = spells.WizardSpells().spells_capacity(level)
        self.spells = {
            "Тайное восстановление": spells.Spells.secret_restoration,
            "Плетение": spells.Spells.weaving,
            "Магическая традиция": spells.Spells.magical_tradition,
            "Знание магов": spells.Spells.mages_knowledge,
        }


# wiz = Wizard("Name", Human, "m", 10)
# print(vars(wiz))