# ────────────────────────────────────────────
# file:        dices.py
# project:     pydnd
# Author:      Timur Barbashov        𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# April 19th 2021
# ────────────────────────────────────────────
from typing import Optional, Union, List
from random import randint


class Dice:
    def __init__(self, type: int, times: int = 1, name: str = ""):
        self.type = type
        if times:
            self.roll_times = times
        else:
            self.roll_times = 1
        if name:
            self.name = name

    def roll(self, times: Optional[int] = None) -> Union[int, List[int]]:
        times_to_roll = self.roll_times * times if times else self.roll_times
        if times_to_roll == 1:
            return randint(1, self.type)
        else:
            return [randint(1, self.type) for _ in range(times_to_roll)]

    def __str__(self) -> str:
        return f"{self.roll_times}D{self.type} '{self.name if self.name else ''}'"

    def __repr__(self) -> str:
        return f"Dice {self.type} with {self.roll_times} rolls by default{' named '+self.name if self.name else ''}"


class d4(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(4, self.times, name)


class d6(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(6, times, name)


class d8(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(8, times, name)


class d10(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(10, times, name)


class d12(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(12, times, name)


class d20(Dice):
    times: int

    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(20, times, name)


# TODO переписать roll(): переписать на DRY, сделать 100% для raw_format=True
class d100(Dice):
    times: int
    # repr и roll
    def __init__(self, times: int = 1, name: str = ""):
        self.times = times
        super().__init__(100, times, name)

    def roll(
        self, times: Optional[int] = None, raw_format: bool = False
    ) -> Union[int, List]:
        times_to_roll = self.roll_times * times if times else self.roll_times
        if not raw_format:
            if times_to_roll == 1:
                return randint(0, 100)
            else:
                return [randint(0, 100) for _ in range(times_to_roll)]
        else:
            if times_to_roll == 1:
                return [(randint(0, 10), randint(0, 10))]
            else:
                return [(randint(0, 10), randint(0, 10)) for _ in range(times_to_roll)]