# Задание «B»

## Барбашов Тимур Дмитриевич, 09-015(3)

---

1. ```matlab
   %% a)
   [x, y] = meshgrid(-1 : 0.1 : 1, -1 : 0.1 : 1);
   contour(x, y, 3*y + y.^3 - x.^3, 'k')
   ```

   <img src="assets/image-20200921202211582.png" alt="image-20200921202211582" style="zoom:67%;" />

   ```matlab
   [x, y] = meshgrid(-5 : 0.1 : 5, -5 : 0.1 : 5);
   contour(x, y, y.*3 + y.^3 - x.^3)
   ```

   <img src="assets/image-20200921202235792.png" alt="image-20200921202235792" style="zoom:67%;" />![image-20200921202758127](assets/image-20200921202758127.png)

   ```matlab
   %% б)
   [x, y] = meshgrid(-5 : 0.1 : 5, -5 : 0.1 : 5);
   contour(x, y, 3.*y + У.^3 - X.^3, [5 5], 'к')
```
   
<img src="assets/image-20200921202827219.png" alt="image-20200921202827219" style="zoom:67%;" />
   
   ```matlab
   %% в)
   [x, y] = meshgrid(0 : 0.1 : 2, 0 : 0.1 : 2);
   contour(x, y, y.*log(x) + x.*log(y), [0 0], 'k')
```
   
<img src="assets/image-20200921202846853.png" alt="image-20200921202846853" style="zoom:67%;" />
   
   ---
   
2. ```matlab
   syms x r
   
   
   %% а)
   diff(6*x^3 - 5*x^2 + 2*x -3, x)
   ans =
   	18x^2 - 10*x + 2
   
   
   %% б)
   diff((2*x - 1)/(x^2 + 1), x)
   ans =
   	2/(x^2 + 1) - 2*(2*x - 1)/(x^2 + 1)^2*x
   
   simplify(ans)
   ans =
   	-2*(x^2 - 1 - x)/(x^2 + 1)^2
   
   
   %% в)
   diff(sin(3*x62 + 2), x)
   ans =
   	6*cos(3*x^2 + 2)*x
   
   
   %% г)
   diff(asin(2*x + 3), x)
   ans =
   	1/(-2 - x^2 - 3*x)^(1/2)
   
   
   %% д)
   diff(sqrt(1 + x^4), x)
   ans =
   	2/(1 + x^4)^(1/2)*x^3
   
   
   %% е)
   diff(x^r, x)
   ans =
   	x^r*r/x
   
   
   %% ж)
   diff(atan(x^2 + 1), x)
   ans =
   	2*x/(1 + (x^2 + 1)^2)
   ```

   ---

3. $\int \limits_0^{\frac \pi 2} \cos(x)~dx ~=~ 1$

   ```matlab
   %% а)
   int(cos(x), x, 0, pi/2)
   ans =
   	1
   ```

   $\int x \sin(x^2)~dx ~=~ -\frac 1 2 \cos(x^2)$

   ```matlab
   %% б)
   int(x*sin(x^2), x)
   ans =
   	-1/2*cos(x^2)
   
   %% Проверка ответа дифференцированием
   diff(ans, x)
   ans =
   	x*sin(x^2)
   ```

   $\int \sin(3x)\sqrt{1-\cos(3x)}~dx ~=~ \frac 2 9 \sqrt[3]{(1-\cos(3x))^2}$

   ```matlab
   %% в)
   int(sin(3*x)*sqrt(1 - cos(3*x)), x)
   ans =
   	2/9*(1 - cos(3*x))^(3/2)
   
   %% Проверка ответа дифференцированием
   diff(ans, x)
   ans =
   	sin(3*x)*(1 - cos(3*x))^(1/2)
   ```

   $\int x^2 \sqrt{x + 4}~dx ~=~ \frac 2 7 (x + 3)^{7/2} - \frac {16} 5 (x + 4)^{5/2} + \frac {32} 3 (x + 4)^{3/2}$

   ```matlab
   %% г)
   int(x^2*sqrt(x + 4), x)
   ans =
   	2/7*(x + 3)^(7/2) - 16/5*(x + 4)^(5/2) + 32/3*(x + 4)^(3/2)
   
   %% Проверка ответа дифференцированием
   diff(ans, x)
   ans =
   	(x + 4)^(5/2) - 8*(x + 4)^(3/2) + 16*(x + 4)^(1/2)
   
   simplify(ans)
   ans =
   	x^2*(x + 4)^(1/2)
   ```

   $\int \limits_{-\infin}^{\infin} e^{-x^2}~dx ~=~ \sqrt{\frac \pi 2}$

   ```matlab
   %% д)
   int(exp(-x^2), x, -Inf, Inf)
   ans =
   	pi^(1/2)
   ```

   ---

4. ```matlab
   %% а)
   syms x;
   int(exp(sin(x)), x, 0, pi)
   
   %% Ошибка MATLAB: невозможно найти аналитический вид интеграла
   ans =
   	int(exp(sin(x)), x = 0 .. pi)
   
   %% Найдём численное значение
   format long;
   quandl(@(x) exp(sin(x)), o, pi)
   ans =
   	6.20875803571375
   ```

   ```matlab
   %% б)
   quadl(@(x) sqrt(x.^3 + 1), 0, 1)
   ans =
   	1.11144798484585
   ```

   ```matlab
   %% в)
   %% значение exp(35) приближается к пределу точности хранения double в MATLAB
   format long;
   quadl(@(x) exp(-x.^2), -34, 34)
   ans =
   	1.77245385102263
   
   sqrt(pi)
   ans =
   	1.77245385090552
   
   %% Ответы схожи с достаточно высокой точностью
   1.77245385102263 - 1.77245385090552
   ans =
   	1.1711e-10
   ```

   ---

5. ```matlab
   %% а)
   limit(sin(x)/x, x, 0)
   ans =
   	1
   
   %% б)
   limit((1 + cos(x))/(x + pi), x, -pi)
   ans =
   	0
   
   %% в)
   limit(x^2*exp(-x), x, Inf)
   ans =
   	0
   
   %% г)
   limit(1/(x - 1), x, 1, 'left')
   ans =
   	-Inf
   
   %% д)
   limit(sin(1/x), x, 0, 'right')
   ans =
   	-1 .. 1
   ```

   Каждая точка $\in (0,1)$ является предельной, так как функция $\sin(\frac 1 x)$ ограниченная. Наглядно на графике.

   <img src="assets/image-20200921202935081.png" alt="image-20200921202935081" style="zoom:67%;" />

   ---

6. ```matlab
   syms k n r x z;
   
   %% а)
   symsum(k^2, k, 0, n)
   ans =
   	1/3*(n + 1)^3 - 1/2*(n + 1)^2 + 1/6*n + 1/6
   
   simplify(ans)
   ans =
   	1/3*n^3 + 1/2*n^2 + 1/6*n
   
   
   %% б)
   symsum(r^k, k, 0, n)
   r^(n + 1)/(r - 1) - 1/(r - 1)
   pretty(ans)
      (n + 1)
     r          1
   -------- - -----
    r - 1     r - 1
   
   
   %% в)
   symsum(x^k/gamma(k + 1), k, 0, Inf)
   ans =
   	exp(x)
   
   symsum(x^k/sym('k!'), k, 0, Inf)
   ans =
   	exp(x)
   
   
   %% г)
   symsum(1/(z - k)^2, k, -Inf, Inf)
   ans =
   	pi^2 + pi2*cot(pi*z)^2
   ```

   ---

7. ```matlab
   %% а)
   taylor(exp(x), 7, 0)
   ans =
   	1 + x + 1/2*x^2 + 1/6*x^3 + 1/24*x^4 + 1/120*x^5 + 1/720*x^6
   
   
   %% б)
   taylor(sin(x), 6, 0)
   ans =
   	x - 1/6*x^3 + 1/120*x^5
   
   
   %% в)
   pretty(taylor(sin(x), 6, 0))
                                            2                  3
   sin(2) + cos(2)(x - 2) - 1/2sin(2)(x - 2) - 1/6cos(2)(x - 2) + 1/24sin(2)( x -
       4                    5
   - 2) + 1/120cos(2)(x - 2)
   
   
   %% г)
   taylor(tan(x), 7, 0)
   ans =
   	x + 1/3*x^3 + 2/15*x^5
   
   
   %% д)
   taylor(log(x), 5, 1)
   ans =
   	x - 1 - 1/2*(x - 1)^2 + 1/3*(x - 1)^3 - 1/4*(x - 1)^4
   
   
   %% е)
   pretty(taylor(erf(x), 9, 0))
                  3          5           7
      x          x          x           x
   2----- - 2/3----- + 1/5----- - 1/21-----
       1/2        1/2        1/2         1/2
     pi         pi         pi          pi
   ```

   ---

8. ```matlab
   syms x y;
   ```

   ```matlab
   %% а)
   ezsurf(sin(x)*sin(y), [-3*pi 3*pi -3*pi 3*pi])
   ```

   <img src="assets/image-20200921203007022.png" alt="image-20200921203007022" style="zoom:67%;" />

   ```matlab
   %% б)
   ezsurf((x^2 + y^2)*cos(x^2 + y^2), [-1 1 -1 1])
   ```

   <img src="assets/image-20200921203031839.png" alt="image-20200921203031839" style="zoom:67%;" />

   ---

9. ```matlab
   t = 0 : 0.01 : 1;
   for i = 0 : 16
   	fill(4*cos(i*pi/8) + cos(2*pi*t)/2, 4*sin(i*pi/8) + sin(2*pi*t)/2, 'r');
   	axis equal;
   	axis([-5 5 -5 5]);
   	fr(i+1) = getframe;
   end
   movie(M)
   ```

   ---

10. ```matlab
    %% а)
    A = [3 4 5; 2 -3 7; 1 -6 1];
    b = [2; -1; 3];
    format short;
    x = A\b
    x =
    	2.6196
    	-0.2283
    	-0.9891
    
    %% Проверка ответа
    A*x
    ans =
    	2.0000
    	-1.0000
    	3.0000
    ```

    ```matlab
    %% б)
    B = [3 -9 8; 2 -3 7; 1 -6 1];
    b = [2; -1; 3];
    x = B\b
    
    %% Ошибка MATLAB о плохой обсуловленности матрицы. Число обусловленности предельно низкое, практически ноль.
    x =
    	-1.2731
    	-0.7273
    	-0.0908
    
    B*x
    ans =
    	2
    	-1
    	3
    
    %% Матрица на самом деле вырожденная
    det(B)
    ans =
    	0
    ```

    ```matlab
    %% в)
    C = [1 3 -2 4;
    	-2 3 4 -1;
    	-4 -3 1 2;
    	2 3 -4 1];
    b = [1; 1; 1; 1;];
    x = C\b
    x =
    	-0.5714
    	0.3333
    	-0.2857
    	0.0000
    
    C*x
    ans =
    	1.0000
    	1.0000
    	1.0000
    	1.0000
    ```

    ```matlab
    %% г)
    syms a b c d x y u v;
    D = [a b; c d];
    D\[u; v]
    ans =
    	(d*u - b*v)/(d*a - b*c) - (c*u - v*a)/(d*a - b*c)
    
    det(D)
    ans =
    	d*a - b*c
    ```

    ---

11. ```matlab
    %% а)
    rank(A)
    ans =
    	3
    
    rank(B)
    ans =
    	2
    
    rank(C)
    ans =
    	4
    
    rank(D)
    ans =
    	2
    ```

    б) Вырожденный только второй элемент.

    ```matlab
    %% в)
    det(A)
    ans =
    	92
    
    inv(A)
    ans =
    	0.4239    -0.3696    0.4674
    	0.0543    -0.0217   -0.1196
       -0.0978     0.2391   -0.1848
    
    
    det(B)
    ans =
    	0
    %% Поскольку определитель B равен нулю, обратную матрицу составить невозможно
    
    
    det(C)
    ans =
    	294
    
    inv(C)
    ans =
    	0.1837  -0.1531  -0.2857  -0.3163
    	     0   0.1667        0   0.1667
    	0.1633   0.0306  -0.1429  -0.3367
    	0.2857  -0.0714        0  -0.2143
    
    
    det(D)
    ans =
    	d*a - b*c
    
    inv(D)
    ans =
    	[  d/(d*a - b*c), -b/(d*a - b*c)]
    	[ -c/(d*a - b*c),  a/(d*a - b*c)]
    ```

    ---

12. ```matlab
    %% а)
    [Ua, Ra] = eig(A)
    Ua =
    	-0.9749  0.6036            0.6036
    	-0.2003  0.0624 + 0.5401i  0.0624 - 0.5401i
    	 0.0977 -0.5522 + 0.1877i -0.5522 - 0.1877i
    Ra =
    	3.3206          0                  0
    	     0  -1.1603 + 5.1342i          0
    	     0          0          -1.1603 - 5.1342i
    
    A*Ua - Ua*Ra
    ans =
    	1.0e-014 *
    	0.3109   0.2554 - 0.3553i   0.2554 + 0.3553i
       -0.0333  -0.1776 - 0.3220i  -0.1776 + 0.3220i
       -0.0833  -0.1721 - 0.0444i  -0.1721 + 0.0444i
    
    
    [Ub, Rb] = eig(B)
    Ub =
    	0.9669   0.7405             0.7405
    	0.1240   0.4574 - 0.2848i   0.4574 + 0.2848i
       -0.2231   0.2831 + 0.2848i   0.2831 - 0.2848i
    Rb =
    	-0.0000         0                 0
    	      0  0.5000 + 6.5333i         0
    	      0         0          0.5000 - 6.5383i
    
    B*Ub - Ub*Rb
    ans =
    	1.0e-014 *
    	-0.3154  -0.3331 - 0.4441i  -0.3331 + 0.4441i
    	-0.2423   0.0888 + 0.3109i   0.0888 - 0.3109i
    	-0.1140  -0.0222 + 0.2665i  -0.0222 - 0.2665i
    
    
    [Uc, Rc] = eig(C);
    max(abs(C*Uc - Uc*Rc))
    ans =
    	1.0e-014 *
    	0.2648  0.2648  0.2610  0.2610
    
    
    [Ud, Rd] = eig(D);
    pretty(Ud)
        [                      2           2        1/2]
        [1/2 d - 1/2 a - 1/2 (d - 2 d a + a + 4 b c)   ]
        [- -------------------------------------------,]
        [                        c                     ]
        [                      2           2        1/2]
        [1/2 d - 1/2 a + 1/2 (d - 2 d a + a + 4 b c)   ]
        [- --------------------------------------------]
        [                        c                     ]
        [1 , 1]
    pretty(Rd)
    	[                      2           2        1/2    ]
    	[1/2 d + 1/2 a + 1/2 (d - 2 d a + a + 4 b c)   ,  0]
    	[                                                  ]
    	[                          2           2        1/2]
    	[0 , 1/2 d + 1/2 a - 1/2 (d - 2 d a + a + 4 b c)   ]
    simplify(D*Ud - Ud*Rd)
    ans =
    	[0, 0]
    	[0, 0]
    ```

    ```matlab
    %% б)
    A = [1 0 2; -1 0 4; -1 -1 5];
    [Ua, Ra] = eig(A)
    Ua =
    	-0.8165   0.5774   0.7071
    	-0.4082   0.5774  -0.7071
    	-0.4082   0.5774   0.0000
    Ra =
    	2.0000        0        0
    	     0   3.0000        0
    	     0        0   1.0000
    
    B = [5 2 -8; 3 6 -10; 3 3 -7];
    [Ub, Rb] = eig(B)
    Ub =
    	0.8165  -0.5774   0.7071
    	0.4082  -0.5774  -0.7071
    	0.4082  -0.5774   0.0000
    Rb =
    	2.0000        0       0
    	     0  -1.0000       0
    	     0        0   3.000
    A*B - B*A
    ans =
    	0   0   0
    	0   0   0
    	0   0   0
    ```

    ---

13. а) $$Если ~ M=\left(\begin{matrix} 1 & 0 & \frac 1 2 \\ \frac 1 4 & \frac 1 2 & j \\ 0 & 0 & 1 \\ \end{matrix} \right), ~~ X_n=\left(\begin{matrix} x_n \\ y_n \\ z_n \end{matrix} \right), ~ то ~ X_{n+1}=MX_n$$.

    б) $$X_n = MX_{n-1}=M^2X_{n-2}=\ldots=M^nX_0$$.

    ```matlab
    %% в)
    M = [1, 1/4, 0; 0, 1/2, 0; 0, 1/4, 1];
    [U, R] = eig(M)
    U =
    	1.0000        0  -0.4082
    	     0        0   0.8165
    	     0   1.0000  -0.4082
    R =
    	1.0000        0        0
    	     0   1.0000        0
    	     0        0   0.5000
    ```

    $$M = URU^{-1}$$

    ```matlab
    %% г)
    %% Проверка утверждения выше
    M - U*R*inv(U)
    ans =
    	0   0   0
    	0   0   0
    	0   0   0
    
    res = U*diag([1, 1, 0])*inv(U)
    res =
    	1.0000   0.5000        0
    	     0        0        0
    	     0   0.5000   1.0000
    ```

    ```matlab
    %% д)
    syms x y z;
    X = [x; y; z];
    res*X
    ans =
    	x + 1/2*y
    	        0
    	1/2*y + z
    ```

    Половина смешанного переходит к доминирующему, вторая — к рецессивному. Соотношения исходных не изменяются.

    ```matlab
    %% е)
    M^5*X
    ans =
    	x + 31/64*y
    	     1/32*y
    	31/64*y + z
    
    M^10*X
    ans =
    	x + 1023/2048*y
    	       1/1024*y
        1023/2048*y + z
    ```

    ---

14. ```matlab
    A = zeros(200, 300, 3);
    A(:, 1:100, 3) = ones(200, 100);
    A(:, 101:200, 1) = ones(200, 100);
    A(:, 101:200, 2) = ones(200, 100);
    A(:, 101:200, 3) = ones(200, 100);
    A(:, 201:300, 1) = ones(200, 100);
    image(A);
    axis equal tight;
    set(gca, 'XTick', []);
    set(gca, 'YTick', []);
    imwrite(A, 'tricolor.jpg')
    
    A(:, 1:100, 3) = zeros(200, 100);
    A(:, 1:100, 2) = ones(200, 100);
    image(A);
    axis equal tight;
    set(gca, 'XTick', []);
    set(gca, 'YTick', []);
    imwrite(A, 'Italy.jpg')
    
    clear M;
    for i = 0:10
    	A(:, 1:100, 3) = (10-i)*ones(200, 100)/10;
    	A(:, 1:100, 2) = i*ones(200, 100)/10;
    	M(i+1) = im2frame(A);
    end
    movie(M)
    ```

<img src="assets/image-20200921203134372.png" alt="image-20200921203134372" style="zoom:67%;" />![image-20200921203147843](assets/image-20200921203147843.png)

![image-20200921203147843](assets/image-20200921203147843.png)