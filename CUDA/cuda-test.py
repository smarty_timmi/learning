# -*- coding:utf-8 -*-
# ────────────────────────────────────────────
# file:        cuda-test.py
# project:     CUDA
# author:      Timur Barbashov            𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# November 4, 2020
# ────────────────────────────────────────────

import pycuda.autoinit
import pycuda.driver as cuda
import numpy as np
from pycuda.compiler import SourceModule
from pprint import pprint
import pycuda.curandom as curandom
from pytools import Table


def task_1_aka_get_info():
    for key, value in cuda.Device(0).get_attributes().items():
        print(f"{key}  ::  {value}")


def task_2():
    kernel = SourceModule(
        """
    __global__ void sum(float *dest, float *a, float *b)
    {
        const int i = threadIdx.x;
        dest[i] = a[i] + b[i];
    }
    """
    )
    sum_gpu = kernel.get_function("sum")
    a = np.random.randn(10).astype(np.float32)
    b = np.random.randn(10).astype(np.float32)
    dest = np.zeros_like(a)
    sum_gpu(cuda.Out(dest), cuda.In(a), cuda.In(b), block=(100, 1, 1), grid=(1, 1))
    print(f"a = {a}\nb = {b}")
    print(f"result - (a+b) = {dest - (a + b)}")


def task_3():
    # 1. Скорость копирования данных

    # 2. Вычисление числа π по формуле площади
    PI_25 = 3.141592653589793238462643
    kernel = SourceModule(
        """
    __global__ void compute_intervals(float *data, int cntSteps, int cntThreads, float step)
    {
        float x;
        float sum = 0.0;
        int threadID = blockDim.x * blockIdx.x + threadIdx.x;
        int cntStepsPerThread = cntSteps / cntThreads;
        int localmax = (threadID + 1) * cntStepsPerThread;
        if (threadID == cntThreads - 1)
            localmax = cntSteps;
        for (int i = threadID * cntStepsPerThread; i < localmax; i++)
        {
            x = (i + 0.5) * step;
            sum = sum + 4.0 / (1.0 + x*x);
        }
        data[threadID] = sum;
    }
    """
    )
    compute_pi = kernel.get_function("compute_intervals")
    rect_count = np.int32(100000)
    step = np.float32(1.0 / rect_count)
    threads_num = 256
    blocks_num = 256
    threads_total = np.int32(threads_num * blocks_num)
    data = np.zeros(threads_total).astype(np.float32)
    compute_pi(
        cuda.InOut(data),
        cuda.In(rect_count),
        cuda.In(threads_total),
        cuda.In(step),
        block=(threads_num, 1, 1),
        grid=(blocks_num, 1),
    )

    pprint(data)
    pprint(np.sum(data) * step)


def test():
    import pycuda.gpuarray as gpuarray

    sizes = []
    times = []
    flops = []
    flopsCPU = []
    timesCPU = []

    for power in range(2, 11):
        size = 2 ** power
        print(f"start at size = {size}")
        sizes.append(size)
        a = gpuarray.zeros((size,), dtype=np.float32)

        count = 2 ** power

        # старт таймера
        start = cuda.Event()
        end = cuda.Event()
        start.record()

        # curand для заполнения случайными числами
        for i in range(count):
            curandom.rand((size,))

        # остановка таймера
        end.record()
        end.synchronize()

        # затраченное время
        secs = start.time_till(end) * 10 ** (-3)

        times.append(secs / count)
        flops.append(size)

        # выделение памяти
        a = np.array((size,), dtype=np.float32)

        # старт таймера
        start = cuda.Event()
        end = cuda.Event()
        start.record()

        # случаные числа на cpu
        for i in range(count):
            np.random.rand(size).astype(np.float32)

        # остановка таймера
        end.record()
        end.synchronize()

        # затраченное время
        secs = start.time_till(end) * 10 ** (-3)

        timesCPU.append(secs / count)
        flopsCPU.append(size)

    # псевдо-флопсы
    flops = [f / t for f, t in zip(flops, times)]
    flopsCPU = [f / t for f, t in zip(flopsCPU, timesCPU)]

    tbl = Table()
    tbl.add_row(
        (
            "Size",
            "GPU",
            "Size/Time GPU [p-FLOPS]",
            "CPU",
            "Size/Time CPU [p-FLOPS]",
            "GPU vs CPU",
        )
    )
    for s, t, f, tCpu, fCpu in zip(sizes, times, flops, timesCPU, flopsCPU):
        tbl.add_row((s, t, f, tCpu, fCpu, f / fCpu))
    print(tbl)


# test()
task_3()
