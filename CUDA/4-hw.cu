// ����� 4. �������� ������

// ���������� ����������
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iomanip>
#include <time.h>
#include <iostream>

// ���������� ������������ ��� std
using namespace std;

// �������� �� ������ ���������� ������� �� CUDA API
void check_cuda_error(const char* message)
{
	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess)
		printf("ERROR: %s: %s\n", message, cudaGetErrorString(err));
}

// ���������� ������� initState, ���������� � ������������� �� GPU
__device__ curandState initState() {
	//int id = threadIdx.x;
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	curandState state;
	curand_init(id, id, 0, &state);
	return state;
}

// ���������� ������� monteCarlo, ���������� �� CPU � ������������� �� GPU
__global__ void monteCarlo(int* inCircle, int maxSize)
{
	curandState state = initState();

	float x = curand_uniform(&state) * 1;
	float y = curand_uniform(&state) * 1;

	if (sqrt(float(pow(x, 2) + pow(y, 2))) < 1) 
	{
		atomicAdd((inCircle + threadIdx.x), 1);
	}
}

int main(int argc, char** argv) 
{
	// ������ �����
	dim3 gridSize(256);

	// ������ �����
	dim3 blockSize(256);

	// ���������� �����
	int threadsCount = blockSize.x * blockSize.y * blockSize.z;

	// ���������� �����, �������� � ����
	int* hostInCircle = new int[threadsCount];

	// ����� ���������� �����, ���������� � �������� [0,1]x[0,1]
	int hostInSquare = gridSize.x * gridSize.y * gridSize.z * blockSize.x * blockSize.y * blockSize.z;

	// ������� ����� "���������" ������ �����
	int* deviceInCircle;

	float elapsedTime;
	cudaEvent_t start, stop;

	// ������ �������
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// �������� ������ �� GPU
	cudaMalloc((void**)&deviceInCircle, threadsCount * sizeof(int));
	check_cuda_error("Allocating memory on GPU");

	cudaEventRecord(start, 0);

	// ����� ������� �� ����������
	monteCarlo << <gridSize, blockSize >> > (deviceInCircle, threadsCount);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	// �������� ������ � GPU �� CPU
	cudaMemcpy(hostInCircle, deviceInCircle, threadsCount * sizeof(int), cudaMemcpyDeviceToHost);
	check_cuda_error("Copying results from GPU");

	// ������������ ���������� ��������� � ������ ������
	int hostInCircleTotal = 0;

	for (int i = 0; i < threadsCount; i++) {
		hostInCircleTotal += *(hostInCircle + i);
	}

	// ��������� ����� pi = 4 * m/n, 
	//��� m - ���������� �����, �������� � ����, n - ���������� ���� ���������� �����
	float pi = (4 * float(hostInCircleTotal) / float(hostInSquare));
	printf("PI: %.12f \n", pi);

	// �����������
	printf("error = %.12f \n", abs(pi - atan(1) * 4));

	// ����� ������������ �������
	cudaEventElapsedTime(&elapsedTime, start, stop);
	printf("elapsedTime: %f millseconds\n", elapsedTime);

	//������������ ������
	cudaFree(deviceInCircle);

	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	return 0;
}
