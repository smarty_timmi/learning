#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>

// ����������� ��� �� ����� 3

// ������� ���������� � CPU � ����������� �� GPU
__global__ void HelloWorld()
{
	printf("Hello world, %d, %d\n", blockIdx.x, threadIdx.x);
}

int myMyMain()
{
	// ���_������� <<<dimGrid, dimBlock>>>
	// ���������: ������ ����� � ������ ����� 
	HelloWorld << <2, 5 >> > ();

	// ���� ������� ���������� ������ �������
	cudaDeviceSynchronize();

	// ������� ������� ����� �������
	getchar();

	return 0;
}


// ����
__global__ void add(int* a, int* b, int* c) {
	*c = *a + *b;
}


//������� �������
int myMain()
{
	// ���������� �� CPU
	int a, b, c;

	// ���������� �� GPU
	int* dev_a, * dev_b, * dev_c;

	//�����������
	int size = sizeof(int); 

	// �������� ������ �� GPU
	cudaMalloc((void**)&dev_a, size);
	cudaMalloc((void**)&dev_b, size);
	cudaMalloc((void**)&dev_c, size);

	// ������������� ����������
	a = 2;
	b = 7;
	
	// ����������� ���������� � CPU �� GPU
	cudaMemcpy(dev_a, &a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, &b, size, cudaMemcpyHostToDevice);

	// ����� ����
	add << < 1, 1 >> > (dev_a, dev_b, dev_c);

	// ����������� ���������� ������ ���� � GPU �� CPU
	cudaMemcpy(&c, dev_c, size, cudaMemcpyDeviceToHost);

	// ����� ����������
	printf("%d + %d = %d\n", a, b, c);

	// �������� ������ �� GPU
	cudaFree(dev_a);
	cudaFree(dev_b);
	cudaFree(dev_c);

	return 0;
}


int main()
{

	// ���������� �� CPU
	int a;

	// ���������� �� GPU
	int* dev_a;

	//�����������
	int size = sizeof(int);

	// �������� ������ �� GPU
	cudaMalloc((void**)&dev_a, size);

	// ������������� ����������
	a = 2;

	// �������������� �������
	cudaEvent_t start, stop;

	float elapsedTime;

	// ������� �������
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// ������ �������
	cudaEventRecord(start, 0);

	// ����� ��������������� ������� myMain(), ���������� ����������� ������
	for (int i = 0; i <= 1000; i++) {
		// ����������� ���������� � CPU �� GPU
		cudaMemcpy(dev_a, &a, size, cudaMemcpyHostToDevice);
	}

	/*
	// ����� ��������������� ������� myMyMain(), ���������� ����������� ������
	myMyMain();
	*/

	// ����� ����
	cudaEventRecord(stop, 0);

	// �������� ���������� ������ ����
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime, start, stop);

	// ����� ����������
	printf("Time spent executing by the GPU: %.10f millseconds\n", elapsedTime);

	// ����������� �������
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	// �������� ������ �� GPU
	cudaFree(dev_a);

	return 0;
}
