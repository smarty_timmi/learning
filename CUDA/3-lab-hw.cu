// ����� 3. ������������ ������
// �������� ������

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// �����-������� ������ ���: https://ru.wikipedia.org/wiki/�����-�������_������

using namespace std;

#define CUDA_FLOAT float
#define GRID_SIZE 256
#define BLOCK_SIZE 256

// �������� zeta(T)
#define T 2

// �������� ������ ����������
void check_cuda_error(const char *message) {
  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess)
    printf("ERROR: %s: %s\n", message, cudaGetErrorString(err));
}

// ������� pi_calc: host -> device
__global__ void pi_calc(CUDA_FLOAT *res) {
  // "����������" ����� ������
  int n = blockIdx.x * BLOCK_SIZE + threadIdx.x;

  // ���� ���� � ������� n + 1 (������������ �� ����������� ������, �������
  // �����)
  CUDA_FLOAT s = float(1) / pow(double(n + 1), T);

  // ������ ���������� � ���������� ������
  res[n] = s;
}

int main(int argc, char **argv) {
  /*
  // ����� ����������
  cudaSetDevice(DEVICE);
  check_cuda_error("Selecting device");
  */

  // ���������� �� ����������
  CUDA_FLOAT *res_d;

  // ������ ��� �������� ���������� � ������ �����
  CUDA_FLOAT res[GRID_SIZE * BLOCK_SIZE];

  // ��������� ������ �� CPU
  cudaMalloc((void **)&res_d, sizeof(CUDA_FLOAT) * GRID_SIZE * BLOCK_SIZE);
  check_cuda_error("Allocating memory on GPU");

  // ������ ����� � ����� �� GPU
  dim3 grid(GRID_SIZE);
  dim3 block(BLOCK_SIZE);

  // ������ ����
  pi_calc<<<grid, block>>>(res_d);

  // ���� ������� ���������� ������ ����������
  cudaThreadSynchronize();
  check_cuda_error("Executing kernel");

  // ����������� ����������� �� ����
  cudaMemcpy(res, res_d, sizeof(CUDA_FLOAT) * GRID_SIZE * BLOCK_SIZE,
             cudaMemcpyDeviceToHost);
  check_cuda_error("Copying results from GPU");

  // ������������ ������ �� GPU
  cudaFree(res_d);
  check_cuda_error("Freeing device memory");

  // ������������� ����������, � ������� �������� ����������� ��������
  // �����-������� ������ zeta(T)
  CUDA_FLOAT zeta = 0;

  // ������������ ��������, ����������� �� ������������ �������� ��������������
  // ������ �������
  for (int i = 0; i < GRID_SIZE * BLOCK_SIZE; i++) {
    zeta += res[i];
  }

  printf("ZETA = %.12f \n", zeta);

  // �����������
  printf("error = %.12f \n", abs(zeta - atan(1) * 4 * atan(1) * 4 / float(6)));

  return 0;
}
