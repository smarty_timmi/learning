# -*- coding:utf-8 -*-
# ────────────────────────────────────────────
# file:        task-2.py
# project:     СМП
# author:      Timur Barbashov            𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# October 4, 2020
# ────────────────────────────────────────────


# S'ₜ  =    μ⋅(N - S) - (β⋅I⋅S / N)    | Восприимчивые
# E'ₜ  =  -(μ + α)⋅E  + (β⋅I⋅S / N)    | Контактные
# I'ₜ  =    α⋅E - (γ + μ)⋅I            | Инфицированные
# R'ₜ  =    γ⋅I - μ⋅R                  | Воздоровевшие
# Размер популяции, константа
# N   =   S + E + I + R
# Порядок вычисления: S -> E -> I -> R

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Button, Slider
from scipy import integrate


def ds(beta, mu, i, s, n):
    return mu * (n - s) - (beta * i * s / n)


def de(alpha, beta, mu, e, i, s, n):
    return -(mu + alpha) * e + (beta * i * s / n)


def di(alpha, gamma, mu, i, e):
    return alpha * e - (gamma + mu) * i


def dr(gamma, mu, i, r):
    return gamma * i - mu * r


def ode_system(t: np.array, y: np.array, params) -> np.array:
    # s, e, i, r = y
    alpha, beta, gamma, mu = params
    derivs = np.array(
        [
            ds(beta, mu, y[2], y[0], n),
            de(alpha, beta, mu, y[1], y[2], y[0], n),
            di(alpha, gamma, mu, y[2], y[1]),
            dr(gamma, mu, y[2], y[3]),
        ]
    )
    return derivs


def update(val):
    alpha = alpha_slider.val
    beta = beta_slider.val
    gamma = gamma_slider.val
    mu = mu_slider.val
    params = (alpha, beta, gamma, mu)
    solution = integrate.solve_ivp(
        ode_system, (0.0, T), y0, args=(params,), vectorized=True, max_step=1.0 / num
    )
    for j in range(4):
        plots[j].set_ydata(solution.y[j])
    plt.suptitle(
        f"$\\alpha = {alpha:.2f},~ \\beta = {beta:.2f},~ \\gamma = {gamma:.2f},~ \\mu = {mu:.2f}$"
    )
    fig.canvas.draw()


# Константы
alpha, beta, gamma, mu = 1.0, 1.0, 1.0, 1.0

# Начальные значения
n = 1000
T = 1
num = 100
s0, e0, i0 = 0.5 * n, 0.5 * n, 0.0
r0 = n - s0 - i0 - e0
y0 = np.array([s0, e0, i0, r0])
t = np.linspace(0, T, num)
params = (alpha, beta, gamma, mu)

solution = integrate.solve_ivp(
    ode_system, (0.0, T), y0, args=(params,), vectorized=True, max_step=1.0 / num
)

# # Not interactive plotting
# for j, label in enumerate(labels):
#     plt.plot(solution.t, solution.y[j], label=label, linewidth=2)
# labels = (
#     "$S ~ (Восприимчивые)$",
#     "$E ~ (Контактные)$",
#     "$I ~ (Инфицированные)$",
#     "$R ~ (Выздоровевшие)$",
# )
# plt.title("SIER model solution")
# plt.xlabel("$t, время$")
# plt.ylabel("Число людей")
# plt.legend()
# plt.grid()
# plt.show()
# # Not interacrive plotting


# # Interactive plotting
alpha0, beta0, gamma0, mu0 = 1.0, 1.0, 1.0, 1.0
fig, axes = plt.subplots()
plt.subplots_adjust(left=0.1, bottom=0.4)
plots = [None, None, None, None]
labels = (
    "$S ~ (Восприимчивые)$",
    "$E ~ (Контактные)$",
    "$I ~ (Инфицированные)$",
    "$R ~ (Выздоровевшие)$",
)
for j, label in enumerate(labels):
    (plots[j],) = plt.plot(solution.t, solution.y[j], label=label, linewidth=2)
plt.grid()
plt.title("SIER model solution")
plt.suptitle(
    f"$\\alpha = {alpha:.2f}, \\beta = {beta:.2f}, \\gamma = {gamma:.2f}, \\mu = {mu:.2f}$"
)
plt.xlabel("$t, время$")
plt.ylabel("Число людей")
plt.legend()
alpha_axes = plt.axes([0.1, 0.1, 0.65, 0.03])
beta_axes = plt.axes([0.1, 0.15, 0.65, 0.03])
gamma_axes = plt.axes([0.1, 0.2, 0.65, 0.03])
mu_axes = plt.axes([0.1, 0.25, 0.65, 0.03])
alpha_slider = Slider(
    alpha_axes, label="$\\alpha$", valinit=alpha0, valmin=0.0, valmax=1.0
)
beta_slider = Slider(beta_axes, label="$\\beta$", valinit=beta0, valmin=0.0, valmax=1.0)
gamma_slider = Slider(
    gamma_axes, label="$\\gamma$", valinit=gamma0, valmin=0.0, valmax=1.0
)
mu_slider = Slider(mu_axes, label="$\\mu$", valinit=mu0, valmin=0.0, valmax=1.0)

alpha_slider.on_changed(update)
beta_slider.on_changed(update)
gamma_slider.on_changed(update)
mu_slider.on_changed(update)
plt.show()
# # Interactive plotting