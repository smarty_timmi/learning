import sympy as sm

# sm.init_printing(use_unicode=True)

alpha, beta, gamma, mu = sm.symbols("alpha beta gamma mu")
n, t = sm.symbols("N t")
s, e, i, r = [sm.Function(j)("t") for j in ("S", "E", "I", "R")]
ds, de, di, dr = [sm.Function(j)("t").diff("t") for j in ("S", "E", "I", "R")]
ds = mu * (n - s) - (beta * i * s) / n
de = -(mu + alpha) * e + (beta * i * s) / n
di = alpha * e - (gamma + mu) * i
dr = gamma * i - mu * r
# print(f"dS'ₜ = {ds}")
for ex in (ds, de, di, dr):
    print(sm.latex(sm.integrate(ex, t)))