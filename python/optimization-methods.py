import numpy as np
import scipy as sp
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Дихотомия & Пауэлл & Ньютон
# f = (x-14)²
# x ∈ [0, 20], ε = 0.1, x₀ = 0


def f(x):
    return (x - 14.0) ** 2


def df(x):
    return 2.0 * (x - 14.0)


# 1.1 Метод дихотомии
def dichotomy(func, start=0.0, end=20.0, eps=0.1, tol=0.2):
    """
    Метод дихотомии. В данном случае первая итерация отрабатывает "вручную", далее запускается процесс

    Parameters
    ----------
    func : целевая функция
    start : левая граница отрезка
    end : права граница отрезка
    eps : малое число
    tol : float, optional
        точность взята как l = 2⋅ε
        by default 0.2
    """

    def choice():
        nonlocal a, b, y, z, func
        if func(y) <= func(z):
            b = z
        else:
            a = y

    iterations = 1
    a, b = start, end
    y, z = (a + b - eps) / 2.0, (a + b + eps) / 2.0
    choice()
    l = abs(b - a)
    while l > tol:
        iterations += 1
        y, z = (a + b - eps) / 2.0, (a + b + eps) / 2.0
        choice()
        l = abs(b - a)
    return (a + b) / 2.0, iterations


# 1.2 Метод Пауэлла
# Для удосбтва точки изначения функций в списках
#   Упрощает поиск точки минимума и соответствующего значения
def powell(func, x_1=0.0, step=0.1, eps=0.1, delta=0.1):
    def stepping():
        nonlocal fn, x  # , step
        if fn[0] > fn[1]:
            x[2] = x[0] + 2.0 * step
        else:
            x[2] = x[0] - step
        if x[2] < x[0]:
            x[0], x[1], x[2] = x[2], x[0], x[1]

    def approx():
        nonlocal a_1, a_2, fn, x_approx
        a_1 = (fn[1] - fn[0]) / (x[1] - x[0])
        a_2 = ((fn[2] - fn[0]) / (x[2] - x[0]) - a_1) / (x[2] - x[1])
        x_approx = (x[1] + x[0]) / 2.0 - a_1 / (2.0 * a_2)

    x = [x_1, x_1 + step, None]
    fn = [func(x[0]), func(x[1]), None]
    iterations = 1

    stepping()

    fn[2] = func(x[2])
    f_min, idx = min(fn), fn.index(min(fn))
    x_min = x[idx]
    a_1, a_2, x_approx = 0.0, 0.0, 0.0

    approx()

    if x_approx > x[2]:
        x_approx = x[1]
    if np.abs(f_min - func(x_approx)) <= eps and np.abs(x_min - x_approx) <= delta:
        return x_approx, iterations

    while True:
        iterations += 1
        x[0], x[1] = x[0], x[0] + step
        fn[0], fn[1] = f(x[0]), f(x[1])

        stepping()

        fn[2] = func(x[2])
        f_min, idx = min(fn), fn.index(min(fn))
        x_min = x[idx]

        approx()

        if np.abs(f_min - func(x_approx)) <= eps and np.abs(x_min - x_approx) <= delta:
            return x_approx, iterations
        else:
            if not x[0] < x_approx < x[2]:
                x[0] = x_approx
            else:
                # Наилучшей точкой выбирается та, которая доставляет наименьшее значение функции
                x[1] = x_min if func(x_min) < func(x_approx) else x_approx
                x[0], x[2] = x[1] - step, x[1] + 2.0 * step


# 1.3 Метод Ньютона
def newton(func, dfunc, a=0.0, b=20.0, x=10.0, eps=0.1):
    # Обусловлено математически. При данной начальной точке сходимость к точке экстремума
    x = (a + b) / 2.0

    iterations, iterations_max = 1, 10 ** 3
    x_next = x - func(x) / dfunc(x)
    while np.abs(x_next - x) > eps and iterations < iterations_max:
        iterations += 1
        # x = x_next
        # x_next = x - func(x) / df(x)
        # Вычисление очередного приближения, свап значений для продвижения
        x, x_next = x_next - func(x_next) / dfunc(x_next), x
    return x_next, iterations


# 2.1 Метод Хука-Дживса
# x⁰ = (2, 3),  Δx = (2, 2),  α = 2,  ε = 0.1
# f(x, y) = 2(x-7)² + (y-7)²
# min: 0.0 at [7.0, 7.0]


def f2(xy):
    return 2.0 * np.square(xy[0] - 7.0) + np.square(xy[1] - 7.0)


def hook_jeeves(func, x_0=np.array([2.0, 3.0]), eps=0.1):
    def research(x: np.array) -> (bool, np.array):
        found = False
        x_new = x
        f_search = func(x)

        # Более-менее понятный/легантный (имхо) перебор точек
        #   в окрестности Мура I порядка, без доп. модулей и исхищрений
        # Можно не заводить флаг found и возвращать сразу же при нахождении подходящего значения,
        #   но в данном случае ищется наиболее оптимальная точка из всех в окрестности
        for xi in (0.0, dlt_x[0], -dlt_x[0]):
            for yj in (0.0, dlt_x[1], -dlt_x[1]):
                candidate = x + np.array([xi, yj])
                f_new = func(candidate)
                if f_new < f_search:
                    f_search, x_new = f_new, candidate
                    found = True
        return found, x_new

    # Исходная формула для образца: sample = xₖ₊₁ + α(xₖ₊₁ - xₖ)
    #   при α=1 можно переписать короче и попроще
    def search_sample(x_prev: np.array, x_next: np.array) -> (np.array, np.array):
        sample = 2.0 * x_next - x_prev
        search_success, sample = research(sample)
        while search_success:
            x_prev, x_next = x_next, sample
            sample = 2.0 * x_next - x_prev
            search_success, sample = research(sample)
        # Новая, предыдущая
        return sample, x_next

    dlt_x = np.array([2.0, 2.0])
    x_new = np.copy(x_0)
    alpha, iterations = 2.0, 0

    while np.linalg.norm(dlt_x) >= eps:
        research_success, x_new = research(x_0)
        if research_success:
            x_new, x_0 = search_sample(x_0, x_new)
        else:
            dlt_x = np.divide(dlt_x, alpha)
        iterations += 1
    return x_new, iterations


# 2.2 Метод Ньютона 2-го порядка
# x⁰ = (2, 3),  ε = 0.1
# f(x, y) = 2(x-7)² + (y-7)²
# min: 0.0 at [7.0, 7.0]
# ∇f = ( 4(x-7), 2(y-7) )
# ∇²f = 4, 0
#       0, 2
# Гессиан - матрица чисел, пересчёт не требуется, положительно определённая
# (∇²f)⁻¹ = 0.25, 0
#           0,    0.5
# Искать по алгоритму необязательно, так как логически выводится из определения:
#   A⋅A⁻¹ = I
# В качестве нормы возьмём L2-норму (евклидову): ∑√x²
# Вообще говоря, заданная функция - квадратичная, и метод Ньютона сходится на ней за одну итерацию
def newton2(func, x_0=np.array([2.0, 3.0])):
    """
    newton2 Алгоритм минимизации многомерным (n=2) методом Ньютона второго порядка

    Parameters
    ----------
    func :
        Целевая функция
    """

    def grad_f(x, fn):
        grad = np.empty_like(x)
        for i in range(x.shape[0]):
            e = np.abs(x[i]) * np.finfo(np.float32).eps

            # а-ля copy, но без copy
            x0 = 1.0 * x[i]

            f0 = fn(x)
            x[i] += e
            f1 = fn(x)
            grad[i] = np.ndarray.item(np.array([f1 - f0])) / e
            x[i] = x0
        return grad

    def hessian(x, fn):
        N = x.shape[0]
        hess = np.empty((N, N))
        grad_0 = grad_f(x, fn)
        e = np.linalg.norm(grad_0) * np.finfo(np.float32).eps
        for i in range(N):
            x0 = 1.0 * x[i]
            x[i] += e
            grad_1 = grad_f(x, fn)
            hess[:, i] = ((grad_1 - grad_0) / e).reshape(N)
            x[i] = x0
        return hess

    def positive(matrix) -> bool:
        """
        positive
        Проверка всех элементов матрицы на положительность

        Parameters
        ----------
        matrix :
            Матрица для проверки

        Returns
        -------
        bool
            True/False
        """

        return (matrix >= 0.0).all()

    iterations, iterations_max = 0, 10 ** 3

    df = grad_f(x_0, func)
    eps_1 = eps_2 = 0.1
    x_new = np.empty_like(x_0)
    d = np.empty_like(df)
    t = 1.0

    while np.linalg.norm(df) >= eps_1 and iterations <= iterations_max:
        iterations += 1
        hess = hessian(x_0, func)
        hess_inv = np.linalg.inv(hess)
        d = -np.matmul(hess_inv, df) if positive(hess_inv) else -df
        x_new = x_0 + t * d
        if (
            np.linalg.norm(x_new - x_0) <= eps_2
            and np.linalg.norm(func(x_new) - func(x_0)) <= eps_2
        ):
            return x_new, iterations
        else:
            x_0 = x_new
        df = grad_f(x_0, func)

    return x_new, iterations


def f3(xy):
    return np.square(xy[0]) + np.square(xy[1]) - 3.0 * xy[1]


# 3.1 Метод штрафов
# f(x) = x₁² + x₂² - 3x₂ -> min
# -2x₁ + x₂² ≤ 0  --> 2x₁ -  x₂² >= 0
#   x₁ - 2x₂ ≤ 0  --> -x₁ + 2x₂ >= 0
# Уравнения ограничений изменены для корректной постановки задачи
# r -- положительный ненулевой параметр, значение убывает на каждой итерации
#   В принципе, можно использовать счётчик итераций в виде 1/iterations
# Минус метода: требуется определение начальной точки, удовлетворяющей ограничениям


def penalty_method(func, x_0=np.array([3.0, 2.0])):
    def penalty(xy):
        confs = np.array([2.0 * xy[0] - np.square(xy[1]), -xy[0] + 2.0 * xy[1]])
        return np.true_divide(np.ones(confs.shape), confs)

    r = 1.0
    penalter = r * np.sum(penalty(x_0))
    eps = 0.1
    iterations, iterations_max = 0, 10 ** 3
    while penalter >= eps and iterations <= iterations_max:
        penalter = r * np.sum(penalty(x_0))
        target = lambda x: func(x) + penalter
        x_0, _ = hook_jeeves(target, x_0)
        iterations += 1
        r /= iterations
    return x_0, iterations


# 3.2 Метод барьеров
# f(x) = x₁² + x₂² - 3x₂ -> min
# -2x₁ + x₂² ≤ 0  --> 2x₁ -  x₂² >= 0
#   x₁ - 2x₂ ≤ 0  --> -x₁ + 2x₂ >= 0
# Барьерная функция: α(x) = ∑ᵢ R₁(gᵢ) + ∑ⱼ R₂(hᵢ), i ∈ [1,m], j ∈ [m+1,l]
#   Второе слагаемое отсутствует ввиду отсутсвия соответствующих ограничений
# R₁, R₂ -- непрерывные, удовлетворяют условиям:
#   R₁(y>=0) = 0, R₁(y<0) > 0  -->  R₁ = (max{0, -y})ᵖ
#   R₂(y=0) = 0, R₁(y!=0) > 0  -->  R₂ = |y|ᵖ
# На каждой итерации rₖ₊₁ = βrₖ, β>1, β выбрана равной 2


def barriers_method(func, x_0=np.array([4.0, 5.0])):
    def barrier(xy, p=1.0):
        confs = np.array([2.0 * xy[0] - np.square(xy[1]), -xy[0] + 2.0 * xy[1]])
        zeros = np.zeros_like(confs)
        # Стандартные функции numpy обрабатывают массивы поэлементно,
        #   поэтому собственная реализация
        confs = (
            zeros if np.any(np.where(np.max(confs) <= zeros, confs, zeros)) else -confs
        )
        return np.power(confs, p)

    r, beta, eps = 1.0, 1.2, 0.1
    barr = 1.0
    iterations, iterations_max = 0, 10 ** 3
    while barr >= eps and iterations <= iterations_max:
        barr = r * np.sum(barrier(x_0))
        target = lambda x: func(x) + barr
        x_0, _ = hook_jeeves(target, x_0)
        r *= beta
        iterations += 1
    return x_0, iterations


def tests():
    labels = []
    x = np.linspace(0.0, 20.0)
    y = f(x)
    plt.title("$f(x) = (x-14)^2$")
    plt.grid()

    methods_names = ("Дихотомия", "Пауэлл", "Ньютон")
    methods = (dichotomy, powell, newton)
    min_point, min_value, iterations = None, None, None
    for method_name, method in zip(methods_names, methods):
        if method_name == "Ньютон":
            min_point, iterations = method(f, df)
        else:
            min_point, iterations = method(f)
        min_value = f(min_point)
        print(method_name)
        print(
            f"x_min: {min_point}",
            f"f_min: {min_value}",
            f"Число итераций: {iterations}",
            sep="\n",
        )
        print("--------")
        labels.append(f"{method_name}: {min_point:.6f} | {iterations}")
        plt.scatter(min_point, min_value, linewidths=iterations)
    plt.legend(labels)
    plt.plot(x, y, linewidth=2)
    plt.show()

    fig = plt.figure()
    nums = 20
    ax = fig.add_subplot(1, 1, 1, projection="3d")
    x = np.linspace(-20, 20, num=nums)
    y = np.linspace(-20, 20, num=nums)
    x, y = np.meshgrid(x, y)

    methods_multidim_names = ("Ньютон²", "Хук-Дживс")
    methods_multidim = (newton2, hook_jeeves)
    for method_name, method in zip(methods_multidim_names, methods_multidim):
        min_point, iterations = method(f2)
        min_value = f2(min_point)
        print("--------")
        print(method_name)
        print(
            f"x_min: {min_point}",
            f"f_min: {min_value}",
            f"Число итераций: {iterations}",
            sep="\n",
        )
        print("--------")

    z = f2([x, y])
    plt.title("$f(x) = 2(x_0-7)^2 + (x_1-7)^2$")
    ax.plot_surface(x, y, z, cmap="plasma")
    plt.show()

    methods_condition_names = ("Штрафы", "Барьеры")
    methods = (penalty_method, barriers_method)
    for method_name, method in zip(methods_condition_names, methods):
        min_point, iterations = method(f3)
        min_value = f3(min_point)
        print("--------")
        print(method_name)
        print(
            f"x_min: {min_point}",
            f"f_min: {min_value}",
            f"Число итераций: {iterations}",
            sep="\n",
        )
        print("--------")

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection="3d")
    z = f3([x, y])
    plt.title("$f(x) = x_1^2 + x_2^2 - 3x_2$")
    ax.plot_surface(x, y, z, cmap="plasma")
    plt.show()


tests()
