# ────────────────────────────────────────────
# file:        GoL-numpy.py
# project:     python
# Author:      Timur Barbashov        𝑏𝑦 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# e-mail:      barbashovtd@mail.ru        𝑓𝑜𝑟 𝑡ℎ𝑒 𝑝𝑒𝑜𝑝𝑙𝑒
# December 29th 2020
# ────────────────────────────────────────────
# todo Переписать мир как итерабельный объект
# todo Переписать правило проверки на принимающее произвольный код проверки
# todo Добавить разные типы соседства: Мура и фон Неймана (плюс)
# GoL с системой поколений (цветовое распределение в градациях серого)

import os
from copy import deepcopy
from time import sleep
from tkinter import Canvas, Tk, Frame, Button, LEFT, RIGHT
import numpy as np


class World:
    width = 0
    height = 0
    alive_symbol = "\N{MEDIUM BLACK CIRCLE}"
    dead_symbol = "\N{MEDIUM WHITE CIRCLE}"
    states = 2
    cells = np.zeros(shape=(width, height))
    iteration = -1
    metadata = {
        "iteration": 0,
        "world size": (0, 0),
        "cells count": 0,
        "alive cells": 0,
        "dead cells": 0,
        "alive cells %": 0,
        "dead cells %": 0,
    }

    def __init__(self, width=None, height=None):
        if width or height is None:
            self.width = self.height = width or height
        else:
            self.width = width
            self.height = height
        self.cells = np.random.randint(
            low=0, high=self.states, size=(self.width, self.height)
        )
        self.metadata["cells count"] = self.width * self.height
        self.metadata["world size"] = (self.width, self.height)
        self.update_metadata()

    def __str__(self):
        cells_repr = f" {str(self.cells)}"
        for char in "[]":
            cells_repr = cells_repr.replace(char, "")
        cells_repr = cells_repr.replace("1", self.alive_symbol).replace(
            "0", self.dead_symbol
        )
        return cells_repr

    def check_rule(self, x, y):
        alive_in_area = (
            np.sum(
                np.take(
                    np.take(self.cells, [x - 1, x, x + 1], axis=0, mode="wrap"),
                    [y - 1, y, y + 1],
                    axis=1,
                    mode="wrap",
                )
            )
            - self.cells[x][y]
        )

        if not 2 <= alive_in_area <= 3:
            return 0
        else:
            if alive_in_area == 3 and self.cells[x][y] == 0:
                return 1
            else:
                return self.cells[x][y]

    def update_metadata(self):
        self.metadata["iteration"] += 1
        self.metadata["alive cells"] = np.count_nonzero(self.cells)
        self.metadata["dead cells"] = (
            self.metadata["cells count"] - self.metadata["alive cells"]
        )
        self.metadata["alive cells %"] = (
            self.metadata["alive cells"] / self.metadata["cells count"]
        )
        self.metadata["dead cells %"] = 1.0 - self.metadata["alive cells %"]

    def evaluate(self, iterations=1):
        new_cells = np.zeros_like(self.cells)
        for _ in range(iterations):
            for x in range(0, self.width):
                for y in range(0, self.height):
                    new_cells[x][y] = self.check_rule(x, y)
            self.cells = deepcopy(new_cells)
            self.update_metadata()
        del new_cells

    def evolution(self):
        pass

    def get_data(self):
        """
        get_data [summary]
        Размер мира
        Текущее поколение
        Число живых клеток
        Число мёртвых клеток
        """
        metadata_repr = "\n".join(
            [
                f"{item[0]} = {item[1]:{'.2f' if type(item[1])==float else ''}}"
                for item in self.metadata.items()
            ]
        )
        return metadata_repr


# my_world = World(5)
# while (
#     my_world.metadata["alive cells"] != 0
#     and my_world.iteration <= my_world.metadata["cells count"] ** 10
# ):
#     os.system("clear")
#     print(my_world)
#     sleep(0.4)
#     my_world.evaluate()
# print(my_world.get_data())

window_width, window_height = 640, 640
root = Tk()
root.title("That`s MY Game of Life!")
frame = Frame(root, width=window_width, height=window_height)
frame.pack()
canvas = Canvas(frame, width=window_width, height=window_height)
canvas.pack()
canvas.bind("<Button-1>", change_color)
start_btn = Button(root, text="start", command=start_game)
stop_btn = Button(root, text="stop", command=stop_game)
start_btn.pack(side=LEFT)
stop_btn.pack(side=RIGHT)
root.mainloop()