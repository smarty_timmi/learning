import numpy as np
from beautifultable import BeautifulTable
from pprint import pprint

n = 10
h = 1 / n
eps = h ** 5
pprint(eps)


def U_func(x):
    return np.power(x, 4) * np.power(1 - x, 3)


def p_func(x):
    return 1 + np.power(x, 4)


def g_func(x):
    return x + 1


diag1, diag2, diag3, f, y, U, g, a, x = (
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
    np.empty(n + 1),
)
for i in range(n + 1):
    x[i] = i * h

for i in range(1, n + 1):
    a[i] = p_func(i * h)

for i in range(2, n):
    diag1[i] = -a[i]

for i in range(1, n):
    diag2[i] = -(a[i] + a[i + 1] + (h ** 2) * g_func(i * h))

for i in range(1, n - 1):
    diag3[i] = -a[i + 1]

diag1[1] = 0
diag3[n - 1] = 0

for i in range(1, n):
    temp = (
        -2
        * i
        * h ** 2
        * (
            35 * i * h ** 7
            - 81 * i * h ** 6
            + 60 * i * h ** 5
            - 14 * i * h ** 4
            + 21 * i * h ** 3
            - 45 * i * h ** 2
            + 30 * i * h
            - 6
        )
    )
    U[i] = U_func(i * h)
    g[i] = g_func(i * h)
    f[i] = (-temp + g[i] * U[i]) * (h ** 2)


pprint("Метод прогонки")


def sweep():
    P = np.empty(n + 1)
    Q = np.empty(n + 1)

    res = np.empty(n + 1)

    P[2] = diag3[1] / diag2[1]
    Q[2] = -f[1] / diag2[1]

    for i in range(2, n - 1):
        P[i + 1] = diag3[i] / (diag2[i] - diag1[i] * P[i])
        Q[i + 1] = (diag1[i] * Q[i] - f[i]) / (diag2[i] - diag1[i] * P[i])

    res[n - 1] = (f[n - 1] - diag1[n - 1] * Q[n - 1]) / (
        diag1[n - 1] * P[n - 1] - diag2[n - 1]
    )
    for i in range(n - 2, 0, -1):
        res[i] = P[i + 1] * res[i + 1] + Q[i + 1]

    return res


y = sweep()

dif = np.array([np.abs(y[i] - U[i]) for i in range(len(y))])


def norm(y):
    mx = 0
    for i in range(1, n):
        value = (y[i - 1] * diag1[i]) - diag2[i] * y[i] + (diag3[i] * y[i + 1])
        mx = max(mx, abs(f[i] - value))
    return mx


pprint("Метод Якоби")


def Jacobi():
    curr_y = np.ones(n + 1)
    next_y = np.empty(n + 1)
    cnt_iter = 0
    while norm(curr_y) > eps:
        for i in range(1, n):
            temp1 = (diag1[i] * curr_y[i - 1]) / diag2[i]
            temp2 = (diag3[i] * curr_y[i + 1]) / diag2[i]
            temp3 = f[i] / diag2[i]
            next_y[i] = temp1 + temp2 - temp3
        curr_y = next_y
        cnt_iter += 1
    pprint(f"Количество итераций:{cnt_iter}")
    return curr_y


yj = Jacobi()
diff = np.empty(n + 1)
for i in range(1, n - 1):
    diff[i] = np.abs(y[i] - yj[i])
for i in range(1, n):
    pprint(f"{i*h}, {y[i]}, {yj[i]}, {diff[i]}, {eps}")
pprint("Метод нижней Релаксации")


def Relax(w):
    curr_y = np.empty(n + 1)
    next_y = np.empty(n + 1)

    for i in range(1, n):
        curr_y[i] = 1

    cnt_iter = 0
    while norm(curr_y) >= eps:
        for i in range(1, n):
            temp = (
                (diag1[i] * curr_y[i - 1]) / diag2[i]
                + (diag3[i] * curr_y[i + 1]) / diag2[i]
                - f[i] / diag2[i]
            )
            next_y[i] = (1 - w) * curr_y[i] + w * temp
        curr_y = next_y
        cnt_iter += 1

    pprint(f"Количество итераций:{cnt_iter}")
    return curr_y


def unpack(s):
    return [f"{x:.6f}" for x in s]


yr = Relax(0.999)
diff = np.empty(n + 1)
for i in range(1, n - 1):
    diff[i] = np.abs(y[i] - yr[i])
for i in range(1, n - 1):
    pprint(f"{i*h}, {y[i]:.8f}, {yr[i]:.8f}, {diff[i]:.8f}")

my_dict = {
    "ih": unpack(x),
    "yi": unpack(y),
    "yki": unpack(yr),
    "|yi-yki|": unpack(diff),
}

pprint(my_dict)

