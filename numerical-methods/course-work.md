### Постановка задачи

Рассматривается движение двух тел под действием взаимного гравитационного притяжения. Тело массой $M$ находится в центре системы координат xOy, $r=(x(t), y(t))$ определяет расстояние до тела массой $m$.

Исходя из физического смысла производной:

$$ma=F \rightarrow mv'=F \rightarrow mr''=F$$

С учетом формулы силы гравитационного взаимодействия:

$$  a = v'=r'', F=G\frac{mM}{r^2}\\ mr''=G\frac{mM}{r^2} $$

Покомпонентная запись даст систему ОДУ:

$$ r=\sqrt{x^2+y^2},\\r''=(r''_{xx}, r''_{yy})=(x'', y'')=GM(\frac{x}{r^3}, \frac{y}{r^3}) $$

$$x'' = -GM\frac{x}{r^3}, ~~ y’’=-GM\frac{y}{r^3}\\x(0)=1-\varepsilon, ~~~~y(0)=0,\\x'(0)=0, ~~~~~~~~~~y'(0)=\sqrt{GM\frac{1+\varepsilon}{1-\varepsilon}}$$

При заданных таким образом начальных условиях орбита будет иметь вид эллипса с эксцентриситетом $\varepsilon$, период обращения планеты массой $m$ будет равен $T=\frac{2\pi}{\sqrt{GM}}$ .

Таким образом, получаем задачи Коши для ОДУ второго порядка. Для нахождения решения будем использовать метод численного интегрирования Нюстрема 4-го порядка.

---

### Метод Нюстрема

Имеем:

$$y''=f(t,y), \\ y(0)=0, \\y'(0)=y_0;$$

Тогда очередное значение $y_{i+1}$ находится по формуле:

$$y_{i+1}=y_i + hy'_i + h^2\frac{(k_1+2k_2)}{6}, \\ y'_{i+1} = y'_i + h\frac{k_1+4k_2+k_3}{6}, \\k_1=f(t_i, y_i), \\ k_2 = f(t_i+h/2, y_i+\frac{h}{2y'_i}+\frac{h^2}{8k_1}), \\ k_3 = f(t_i+h, y_i+\frac{h}{y'_i}+\frac{h^2}{2k_2})$$

---

### Модельная задача

Для проверки работы метода будем решать модельную задачу:

$$y''_1 = \displaystyle\frac{1}{y_1y_2^2}+\frac{1}{e^x}, \\ y''_2 = \displaystyle\frac{1}{y_2y_1^2}+\frac{1}{e^{-x}},\\ $$

Точные решения известны: $y_1=e^x, ~~ y_2=e^{-x}$

Проверим:

$$ (e^x)''=e^x=\frac{1}{e^x(e^{-x})^2} + \frac{1}{e^x}=2\cosh(x), \\ (e^{-x})''=e^x= \frac{1}{e^{-x}(e^x)^2} + \frac{1}{e^{-x}}=2\cosh(x)$$

Система решается на отрезке [0,3], начальные условия берутся из точных решений системы:

$$y_1(0) = 1, ~~ y_2(0)=1, \\ y'_1(0)=1, ~~ y'_2(0) = -1$$

Для численного решения задачи была написана программа на языке Python.

Входные данные:

$$(a,b)=(0,3), ~~ \varepsilon \in (0,1), ~~ \alpha=\frac{\pi}{4}$$

---

### Графики решений

Графики решений системы в координатах $(x(t),y(t))$ при различных значениях $\varepsilon$. В случае $\varepsilon=0$ значение бралось максимально малым, допустимым для операционной системы ($\approx 10^{-16}$). 

<img src="course-work.assets/a-lot-of-test-luxury-edition.png" />

Графики чётко показывают зависимость скорости схождения тела меньшей массы к телу с большей массой в зависимости от его начального положения: для каждого графика время одинаково, но при больших $\varepsilon$ тело меньшей массы достигает центра масс быстрее.

### Дополнительные графики

![](course-work.assets/a-lot-of-tests.png)

---

### Точность метода Нюстрема

Метод Рунге-Кутты-Нюстрема как нельзя лучше подходит для данной задачи, так как имеет пятой порядок точности при решении дифференциальных уравнений второго порядка. На графике показаны решения, полученные методом Нюстрема, и известные точные решения задачи.

На оси абсцисс отображено число точек для вычисления $y_i$, по оси ординат – максимальная ошибка $max|u_i-y_i|$.

$u_i$ – точное решение, $y_i$ – вычисленное по методу Нюстрема.

![](course-work.assets/Figure_1_num.png)

На первом графике заметна возрастающая разница погрешность между $u_1,y_1$. Объясняется это тем, что решение задачи – возрастающая функция ($e^t$), в следствие чего точность аппроксимации снижается.

---

### Листинг программы

```python
# -*- coding:utf-8 -*-
# x" = -α x/r³,  y" = -α y/r³,  r = √(x²+y²)
# x(0) = x₀ = 1 - ε,  y(0) = y₀ = 0
# x'(0) = 0,  y'(0) = √( γM(1+ε)/(1-ε) )
#   k₁ = f(t, x, y)
#   k₂ = f(t+h/2, x + h/(2x') + h²/(8k₁), y + h/(2y') + h²/(8k₁))
#   k₃ = f(t+h, x + hx' + h²/(2k₂), y + hy' + h²/(2k₂))
# α = √(γM)

import sys
from functools import wraps
from time import time
import matplotlib.pyplot as plt
import numpy as np


def f(x0, y0):
    nom = -((np.pi / 4) ** 2)
    denom = nom * (x0 ** 2 + y0 ** 2) ** (3 / 2)
    return (x0 / denom, y0 / denom)


def Nystroem(nums, x0, dx_0, y0, dy_0, f, h):
    dx, dy = np.zeros(nums), np.zeros(nums)
    x, y = np.zeros(nums), np.zeros(nums)
    x[0], y[0] = x0, y0
    dy[0], dx[0] = dy_0, dx_0

    # Первая итерация вручную для удобства
    k1 = f(x0, y0)
    k2 = f(
        x0 + h / 2 * dx_0 + h * h / (8 * k1[0]), y0 + h / 2 * dy_0 + h * h / (8 * k1[1])
    )
    k3 = f(x0 + h * dx_0 + h * h / (2 * k2[0]), y0 + h * dy_0 + h * h / (2 * k2[1]))

    def xy_next(x_n, dx_n, y_n, dy_n, k1, k2):
        c = h * h / 6
        return (
            x_n + h * dx_n + c * (k1[0] + 2 * k2[0]),
            y_n + h * dy_n + c * (k1[1] + 2 * k2[1]),
        )

    def diff_xy_next(dx_n, dy_n, k1, k2, k3):
        c = h / 6
        return (
            dx_n + c * (k1[0] + 4 * k2[0] + k3[0]),
            dy_n + c * (k1[1] + 4 * k2[1] + k3[1]),
        )

    for i in range(0, nums - 1):
        x[i + 1], y[i + 1] = xy_next(x[i], dx[i], y[i], dy[i], k1, k2)
        dx[i + 1], dy[i + 1] = diff_xy_next(dx[i], dy[i], k1, k2, k3)
        k1 = f(x[i], y[i])
        c1, c2 = h * h / 8, h * h / 2
        k2 = f(x[i] + h / 2 * dx[i] + c1 / k1[0], y[i] + h / 2 * dy[i] + c1 / k1[1])
        k3 = f(x[i] + h * dx[i] + c2 / k2[0], y[i] + h * dy[i] + c2 / k2[1])
    return (x, y, dx, dy)


def model_test(a=0, b=3, n=50):
    def f1_test(y1, y2):
        return (
            0.5 / (y1 * y2 ** 2) + 0.5 * y1,
            0.5 / (y2 * y1 ** 2) + 0.5 * y2,
        )

    h = (b - a) / n
    t = np.linspace(a, b, n)
    y1, y2, _, _ = Nystroem(n, 1, 1, 1, -1, f1_test, h)
    labels = (r"$y_1$", r"$u_1$", r"$y_2$", r"$u_2$")
    return (labels, y1, y2, np.exp(t), np.exp(-t), h)


def model_tolerance():
    start_n, stop_n, step_n = 10, 1000, 10
    mx_err1, mx_err1_h = np.zeros(int(stop_n / step_n)), np.zeros(int(stop_n / step_n))
    mx_err2, mx_err2_h = np.zeros(int(stop_n / step_n)), np.zeros(int(stop_n / step_n))
    for i, n in enumerate(range(start_n, stop_n + 1, step_n)):
        labels, y1, y2, u1, u2, h = model_test(a, b, n)
        mx_err1[i] = np.max(u1 - y1)
        mx_err1_h[i] = np.max(np.max(u1 - y1) / h ** 4)
        mx_err2[i] = np.max(u2 - y2)
        mx_err2_h[i] = np.max(np.max(u2 - y2) / h ** 4)
    labels = (
        r"$|u_1-y_1|$",
        r"$\frac{|u_1-y_1|}{h^4}$",
        r"$|u_2-y_2|$",
        r"$\frac{|u_2-y_2|}{h^4}$",
    )
    fig, axs = plt.subplots(2, 2)
    (ax1, ax2), (ax3, ax4) = axs
    for ax, eps, label in zip(
        axs.flatten(), (mx_err1, mx_err1_h, mx_err2, mx_err2_h), labels
    ):
        ax.grid()
        ax.set_title(label)
        ax.plot(eps)
    return fig, axs


SHOW_TEST_MODEL = True
SHOW_MODEL_TOLERANCE = True
SHOW_MODEL_TESTS = True

alpha_test = np.pi / 4
a, b, n = 0, 3, 100
h = (b - a) / n
t = np.linspace(a, b, n)
zero = sys.float_info.epsilon
eps = 0.1
x0, y0 = 1.0 - eps, zero
dx_0, dy_0 = zero, alpha_test * np.sqrt((1 + eps) / (1 - eps))

if SHOW_TEST_MODEL == True:
    labels, y1, y2, u1, u2, h = model_test(a, b, n)
    fig, axs = plt.subplots(1, 2, sharex="col", sharey="row")
    ax1, ax2 = axs
    ax1.grid()
    ax1.plot(y1, "b--")
    ax1.plot(u1, "g-")
    ax1.legend(labels[0:2])
    ax2.grid()
    ax2.plot(y2, "b--")
    ax2.plot(u2, "g-")
    ax2.legend(labels[2:4])
    plt.show()
if SHOW_MODEL_TOLERANCE == True:
    fig, axs = model_tolerance()
    plt.show()
m = 4
eps = np.arange(zero, 1.0, 1 / m ** 2)
fig = plt.figure()
axes = fig.subplots(nrows=m, ncols=m)
values = [list() for i in range(m ** 2)]
for i in range(m):
    for j in range(m):
        x0, y0 = 1.0 - eps[m * i + j], zero
        dx_0, dy_0 = (
            zero,
            alpha_test * np.sqrt((1 + eps[m * i + j]) / (1 - eps[m * i + j])),
        )
        x, y, _, _ = Nystroem(n, x0, dx_0, y0, dy_0, f, h)
        values[m * i + j] = x, y
ticks = np.linspace(-1.0, 1.0, 5)
for i in range(m):
    for j in range(m):
        k = m * i + j
        x, y = values[k]
        axes[i, j].plot(x, y)
        axes[i, j].grid()
        axes[i, j].set_title(r"$\varepsilon_{{{k+1}}} \approx {eps[k]:.3f}$")
        axes[i, j].set_xlabel(r"$x(t)$")
        axes[i, j].set_ylabel(r"$y(t)$")
        axes[i, j].set_xbound(-1.0, 1.0)
        axes[i, j].set_ybound(-1.0, 1.0)
        axes[i, j].set_xticks(ticks)
        axes[i, j].set_yticks(ticks)
axes[0, 0].set_title(r"$\varepsilon_0 \approx {zero}$")
plt.show()

plt.plot(t, x, "-b", label=r"$x(t)$")
plt.plot(t, y, "-g", label=r"$y(t)$")
plt.legend()
plt.grid()
plt.show()

```

