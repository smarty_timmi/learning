# -*- coding:utf-8 -*-
# ┌──────────────────────────────────────┐
# │ file:      numerical-triag.py        │
# │ project:   second                    │
# │ created:   October 3rd 2019          │
# │ modified:  Oct 3rd 2019  04:47       │
# │                                      │
# │ author:    Timur Barbashov           │
# │ e-mail:    barbashovtd@mail.ru       │
# └──────────────────────────────────────┘
# Feel free to use in non-commrecial purposes

# SOR + Zeidel
# α = 1, β = 1, γ = 2, ω ∈ (1, 2)
# u(x) = xᵅ⋅(1-x)ᵝ,
# p(x) = 1+xᵞ,
# q(x) = x-1,
# f(x) = -(p(u)u'(x))' + q(x)u(x) = -x³+8⋅x²-3⋅x+2
#    y₀ = 0,
#    (p₁+ p₂ + h²q₁)⋅y₁ - p₂y₂ = f₁h²,
#    ⋅⋅⋅
#      -pᵢyᵢ₋₁ + (pᵢ + pᵢ₊₁ + h²qᵢ)⋅yᵢ - pᵢ₊₁yᵢ₊₁ = fᵢh²,
#    ⋅⋅⋅
#     - pₙ₋₁yₙ₋₂ + (pₙ₋₁ + pₙ + h²qₙ₋₁)⋅yₙ₋₁ = fₙ₋₁h²,
#    yₙ = 0;

import numpy as np
from matplotlib import pyplot as plt


def unpack(s: np.array) -> str:
    return " ".join(f" {x:0.3f} " for x in s)


def u(x, alpha=1, beta=1):
    return x ** alpha * (1 - x) ** beta


def f(x):
    return -(x ** 3) + 8 * x * x - 3 * x + 2


def p(x, gamma=1):
    return 1 + x ** gamma


def q(x):
    return 1.0 + x


# В формальном алгоритме: нумерация с единицы до n [1, n]
def TDMA_Solver(lower, main, upper, fi):
    # a, b, c  --  коэф-ты слева-направо
    def TDMA(a, b, c, f):
        n = len(main)
        P, Q = np.zeros(n), np.zeros(n)
        x = np.zeros(n)
        # i from 2 to n-1
        denom = 1
        for i in range(2, n):
            denom = b[i - 1] - a[i - 1] * P[i - 1]
            P[i] = c[i - 1] / denom
            Q[i] = (a[i - 1] * Q[i - 1] - f[i - 1]) / denom
        x[n - 1] = (a[n - 1] * Q[n - 1] - f[n - 1]) / (b[n - 1] - a[n - 1] * P[n - 1])
        # i from n-1 to 1
        for i in range(n - 2, -1, -1):
            x[i] = P[i + 1] * x[i + 1] + Q[i + 1]
        return x

    def TDMA2(a, b, c, f):
        n = len(f)
        P, Q = [-c[0] / b[0]], [f[0] / b[0]]
        x = np.zeros(n)
        for i in range(1, n - 1):
            g = b[i] + a[i - 1] * P[i - 1]
            P.append(-(c[i] / g))
            Q.append((f[i] - a[i - 1] * Q[i - 1]) / g)
        x[n - 1] = (f[n - 1] - a[n - 2] * Q[n - 2]) / (b[n - 1] + a[n - 2] * P[n - 2])
        for i in range(n - 2, -1, -1):
            x[i] = P[i] * x[i + 1] + Q[i]
        return x

    return TDMA2(lower, main, upper, fi)


def diag3_Matrix(lower, main, upper):
    from scipy.sparse import diags

    n = len(main)
    A = diags([lower, main, upper], [-1, 0, 1], shape=(n, n))
    return A


def r(y, u):
    return np.linalg.norm(y - u)


alpha, beta, gamma = 1, 1, 2
a, b, n = 0, 1, 32
h = 1 / n
h_2 = h ** 2
x = np.linspace(a, b, n)
tolerances = (h ** 3, h ** 4, h ** 5)

# pi, qi_h2, fi_h2, ui = p(x), q(x) * h_2, f(x) * h_2, u(x)
ui = u(x)
fi_h2 = f(x) * h_2
upper = np.array([-p(i * h) for i in range(1, n + 1)])
lower = np.array([-p(i * h) for i in range(1, n + 1)])
main = np.array(
    [p(i * h) + p((i + 1) * h) + q(i * h) * h ** 2 for i in range(1, n + 1)]
)
y = TDMA_Solver(lower, main, upper, fi_h2)
labels = ("Аппроксимация", "Решение")
plt.plot(x, y)
plt.plot(x, ui)
plt.legend(labels)
plt.grid()
plt.show()
A = diag3_Matrix(lower, main, upper)
print(f"{A.toarray()}")


# Нахождение функции f(x)
# from sympy import *
# x = symbols("x")
# a, b, g = 1, 1, 2
# # a, b, g = S(alpha), S(beta), S(gamma)
# u = x ** a * (1 - x) ** b
# p, q = 1 + x ** g, x - 1
# f = -(p * u.diff(x)).diff(x) + q * u
# init_printing(use_unicode=True)
# pretty_print(simplify(f))
