import numpy as np
import matplotlib.pyplot as plt
import math
from beautifultable import BeautifulTable


def U_func(x):
    return x * (1 - x)


def p_func(x):
    return 1 + x * x


def q_func(x):
    return x - 1


n = 10
h = 1 / n
eps = h ** 3

a = np.empty(n + 1)

diag1 = np.empty(n - 1)
diag2 = np.empty(n - 1)
diag3 = np.empty(n - 1)

y = np.empty(n + 1)
f = np.empty(n + 1)
U = np.empty(n + 1)
x = np.empty(n + 1)
g = np.empty(n + 1)

for i in range(n + 1):
    x[i] = i * h

for i in range(n + 1):
    a[i] = p_func(x[i])
    temp = -(x[i] ** 3) + 8 * x[i] * x[i] - 3 * x[i] + 2
    U[i] = U_func(x[i])
    g[i] = q_func(x[i])
    f[i] = -temp + q_func(x[i]) * U[i]

F = np.empty(n - 1)
for i in range(n - 1):
    F[i] = f[i + 1] * h ** 2

y[0] = diag1[0] = 0
y[n] = diag3[n - 2] = 0

diag2[0] = a[1] + a[2] + h * h * g[1]
diag3[0] = -a[2]

for i in range(1, n - 2):
    diag1[i] = -a[i + 1]
    diag2[i] = -(a[i + 1] + a[i + 2] + h * h * g[i])
    diag3[i] = -a[i + 2]

diag1[n - 2] = -a[n - 1]
diag2[n - 2] = a[n - 1] + a[n] + h * h * g[n - 1]


def sweep(A, B, C, F):
    sze = len(F)

    P = np.empty(sze)
    Q = np.empty(sze)

    P[0] = Q[0] = 0

    P[1] = C[0] / B[0]
    Q[1] = -F[0] / B[0]

    res = np.empty(sze)
    for i in range(1, sze - 1):
        P[i + 1] = C[i] / (B[i] - A[i] * P[i])
        Q[i + 1] = (A[i] * Q[i] - F[i]) / (B[i] - A[i] * P[i])

    res[sze - 1] = (A[sze - 1] * Q[sze - 1] - F[sze - 1]) / (
        B[sze - 1] - A[sze - 1] * P[sze - 1]
    )
    for i in range(sze - 1, 0, -1):
        res[i - 1] = P[i] * res[i] + Q[i]

    return res


# -3 1 5 -8

res_s = np.empty(n - 1)
res_s = sweep(diag1, diag2, diag3, F)

for i in range(n - 1):
    y[i + 1] = res_s[i]

dif = np.empty(n + 1)
for i in range(n + 1):
    dif[i] = np.abs(y[i] - U[i])

print(y)
table = BeautifulTable()
table.column_headers = ["ih", "y[i]", "u(ih)", "|y[i] - u(ih)|"]
for i in range(n + 1):
    table.append_row([x[i], y[i], U[i], dif[i]])

print(table)


def norm(y):
    mx = 0
    for i in range(n - 2):
        value = diag1[i] * y[i] - diag2[i] * y[i + 1] + diag3[i] * y[i + 2]
        mx = max(mx, abs(f[i] - value))
    return mx


def Jacobi(a, g, h, f):
    sze = len(f)
    curr_y = np.empty(sze)
    next_y = np.empty(sze)

    cnt_iter = 0

    for i in range(sze):
        curr_y[i] = 1

    while norm(curr_y) >= eps:
        for i in range(1, sze - 1):
            temp1 = (a[i] * curr_y[i - 1]) / (a[i] + a[i + 1] + h ** 2 * g[i])
            temp2 = (a[i + 1] * curr_y[i + 1]) / (a[i] + a[i + 1] + h ** 2 * g[i])
            temp3 = (f[i] * h ** 2) / (a[i] + a[i + 1] + h ** 2 * g[i])

            next_y[i] = temp1 + temp2 + temp3
        curr_y = next_y
        cnt_iter += 1

    print(cnt_iter)
    return curr_y


res_j = np.empty(n + 1)
res_j = Jacobi(a, g, h, f)

dif = np.empty(n + 1)
for i in range(n + 1):
    dif[i] = np.abs(y[i] - res_j[i])

table = BeautifulTable()
table.column_headers = ["ih", "y[i]", "Jacobi", "|y[i] - Jacobi|"]
for i in range(n + 1):
    table.append_row([x[i], y[i], res_j[i], dif[i]])

print(table)


def Relax(a, g, h, f):
    sze = len(f)

    curr_y = np.empty(n + 1)
    next_y = np.empty(n + 1)

    cnt_iter = 0
    w = 0.9

    for i in range(sze):
        curr_y[i] = 0
        next_y[i] = 0

    while True:
        norm = 0
        for i in range(1, sze - 1):
            temp1 = (a[i] * curr_y[i - 1]) / (a[i] + a[i + 1] + h ** 2 * g[i])
            temp2 = (a[i + 1] * curr_y[i + 1]) / (a[i] + a[i + 1] + h ** 2 * g[i])
            temp3 = (f[i] * h ** 2) / (a[i] + a[i + 1] + h ** 2 * g[i])

            I = temp1 + temp2 + temp3
            next_y[i] = (1 - w) * curr_y[i - 1] + w * I

            if abs(curr_y[i] - next_y[i]) > norm:
                norm = abs(curr_y[i] - next_y[i])

        curr_y = next_y.copy()
        cnt_iter += 1
        if norm <= eps:
            break

    print(cnt_iter)
    return curr_y
